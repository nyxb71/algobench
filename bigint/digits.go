package bigint

import "fmt"

type Digit int64
type Digits []Digit

var digitToRune = map[Digit]rune{
	0:  '0',
	1:  '1',
	2:  '2',
	3:  '3',
	4:  '4',
	5:  '5',
	6:  '6',
	7:  '7',
	8:  '8',
	9:  '9',
	10: 'A',
	11: 'B',
	12: 'C',
	13: 'D',
	14: 'E',
	15: 'F',
}

var runeToDigit = map[rune]Digit{
	'0': 0,
	'1': 1,
	'2': 2,
	'3': 3,
	'4': 4,
	'5': 5,
	'6': 6,
	'7': 7,
	'8': 8,
	'9': 9,
	'A': 10,
	'B': 11,
	'C': 12,
	'D': 13,
	'E': 14,
	'F': 15,
}

// Return the sign of the digit (-1, 0, 1)
func (d Digit) Sign() Digit {
	if d > 0 {
		return 1
	}

	if d < 0 {
		return -1
	}

	return 0
}

func (d Digit) Abs() Digit {
	return absDigit(d)
}

// Return the sign of the first digit in the list
// NOTE: assumes no leading zeroes
func (d Digits) Sign() Digit {
	return d[0].Sign()
}

// Convert a standard int to digits
func FromInt(a int64, base Base) Digits {
	da := Digit(a)
	if da.Abs() >= base {
		return Digits{da}
	}

	sign := da.Sign()
	digits := Digits{}

	cur := da
	for cur.Abs() > 0 {
		digits = append(digits, sign*(cur.Abs()%10))
		cur /= 10
	}

	return digits.reversed()
}

func (d Digits) IntStr(base Base) IntStr {
	if len(d) == 0 {
		return IntStr("")
	}

	sign := d.Sign()

	str := ""
	for i := 0; i < len(d); i++ {
		// TODO: Accomodate bases > 16
		digitChar, ok := digitToRune[absDigit(d[i])]
		if !ok {
			panic(fmt.Sprintf("ERROR CONVERTING %d", d[i]))
		}
		if d[i] >= base {
			panic(fmt.Sprintf("%d greater than specified base (%d)", d[i], base))
		}
		str += string(digitChar)

	}

	if sign == -1 {
		return "-" + IntStr(str)
	}
	return IntStr(str)
}

// Adjust the signs in a to match the sign of the first non-zero digit
func (d Digits) normalizeSigns(base Base) Digits {
	if len(d) == 1 {
		return d
	}

	// Make a copy so we don't mutate the original
	c := make(Digits, len(d))
	copy(c, d)

	// Get the sign of the first non-zero digit
	// determines overall sign
	leadingSign := Digit(0)
	for i := 0; i < len(c); i++ {
		if curSign := c[i].Sign(); curSign != 0 {
			leadingSign = curSign
			break
		}
	}

	// Working right to left
	for i := len(c) - 1; i >= 1; i-- {
		// If the sign of the current value != the overall sign
		// (ignoring 0 values)
		curSign := c[i].Sign()
		if curSign != leadingSign && curSign != 0 {
			// Add the signed base to the current value to make it switch signs
			c[i] += leadingSign * base
			// Add the opposite sign to the value to the left
			c[i-1] -= leadingSign
		}
	}

	return c
}

func (d Digits) carryOverflows(base Base) Digits {
	// NOTE: Assumes all digits have the same sign (or are 0)

	newDigits := Digits{}
	carry := Digit(0)

	// Working right to left
	for i := len(d) - 1; i >= 0; i-- {
		// Add the carry to the current value
		cur := d[i] + carry

		// If the resulting value is >= base
		if absDigit(cur) >= base {
			// Set the carry to the number of bases in cur
			carry = cur / base
			// Append the remainder to the new digits
			newDigits = append(newDigits, cur%base)
		} else {
			// Reset the carry
			carry = 0
			// Append cur to the new digits
			newDigits = append(newDigits, cur)
		}
	}

	// Account for the final carry
	if carry != 0 {
		newDigits = append(newDigits, carry)
	}

	// Since we appended digits right to left, they are backwards,
	// and need to be reversed
	return newDigits.reversed()
}

func (d Digits) String(base Base) string {
	return string(d.IntStr(base))
}

func (d Digits) reversed() Digits {
	new := Digits{}
	for i := len(d) - 1; i >= 0; i-- {
		new = append(new, d[i])
	}
	return new
}

func (d Digits) trimLeadingZeroes() Digits {
	if len(d) == 1 {
		return d
	}

	for i := 0; i < len(d); i++ {
		if d[i] != 0 {
			return d[i:]
		}
	}

	return Digits{0}
}

func (d Digits) zeroPad(n int) Digits {
	if n <= len(d) {
		return d
	}

	padded := make(Digits, n)
	start := n - len(d)
	for i := 0; i < len(d); i++ {
		padded[i+start] = d[i]
	}

	return padded
}

func maxDigits(a Digits, b Digits) uint64 {
	if len(a) < len(b) {
		return uint64(len(b))
	}
	return uint64(len(a))
}

func absDigit(n Digit) Digit {
	// http://cavaliercoder.com/blog/optimized-abs-for-int64-in-go.html
	y := n >> 63
	return (n ^ y) - y
}
