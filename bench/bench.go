package bench

import (
	"log"
)

// Options stores options that apply to every algorithm
type Options struct {
	Export    bool
	Print     bool
	NumTrials uint64
	Output    string
}

// HandleRequest accepts a benchmark request, runs it, gets the
// results, and if the export flag is used, saves them to a file.
func HandleRequest(algoName string, algoType string, options Options) {
	var timer Timer
	switch algoType {
	case "search":
		timer = SearchTimers.Map[algoName]
	case "sort":
		timer = SortTimers.Map[algoName]
	case "misc":
		timer = MiscTimers.Map[algoName]
	case "bigint":
		timer = BigIntTimers.Map[algoName]
	case "lcs":
		timer = LCSTimers.Map[algoName]
	case "tsp":
		timer = TSPTimers.Map[algoName]
	}

	if timer.F == nil || timer.InputSteps == nil {
		log.Fatalf("Timer for %s has nil components: %v\n", algoName, timer)
	}

	ts := MakeTrialSeries(algoName, timer.F, options.NumTrials, timer.InputSteps)

	ts.Run(options.Print)

	if options.Export {
		ts.Export(options.Output)
	}
}

// HandleRequests hands off a series of requests to HandleRequest()
func HandleRequests(algoNames []string, algoType string, options Options) {
	if len(algoNames) == 0 {
		log.Fatal("Found no algorithms to bench.")
	}

	for _, algoName := range algoNames {
		HandleRequest(algoName, algoType, options)
	}

}
