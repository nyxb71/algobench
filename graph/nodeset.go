package graph

import (
	"strconv"
)

type NodeSet []int

func (n *NodeSet) Packed() PackedNodeSet {
	return PackNodeSet(*n)
}

func (n *NodeSet) Without(i int) NodeSet {
	new := NodeSet{}
	for _, v := range *n {
		if v != i {
			new = append(new, v)
		}
	}
	return new
}

type PackedNodeSet int

func (pns *PackedNodeSet) Unpack() NodeSet {
	return UnpackNodeSet(*pns)
}

func (pt PackedNodeSet) ToBinary() string {
	return strconv.FormatInt(int64(pt), 2)
}

func PackNodeSet(ns NodeSet) PackedNodeSet {
	packed := PackedNodeSet(0)

	if len(ns) == 0 {
		panic("nodeset must contain at least one node")
	}

	for _, v := range ns {
		packed |= 1 << v
	}
	return packed
}

func PackFromAToB(a int, b int) PackedNodeSet {
	return PackNodeSet([]int{a, b})
}

func UnpackNodeSet(p PackedNodeSet) NodeSet {
	unpacked := NodeSet{}
	i := int(0)

	// For each bit working right to left,
	// if the bit is one, add it's index in the bit field
	// to the NodeSet
	for p > 0 {
		if p&1<<1 != 0 {
			unpacked = append(unpacked, i)
		}
		p >>= 1
		i++
	}

	return unpacked
}
