package bigint

import (
	"reflect"
)

type Base = Digit

type BigInt struct {
	// Each digit is signed
	digits Digits
	base   Base
}

func MakeDefault() BigInt {
	return BigInt{digits: Digits{0}, base: 10}
}

func Make(d Digits, b Base) BigInt {
	return BigInt{digits: d, base: b}
}

func (a *BigInt) Sign() int64 {
	return int64(a.digits.Sign())
}

func (a *BigInt) Base() Base {
	return a.base
}

func (a *BigInt) Len() int {
	return len(a.digits)
}

func Equal(a BigInt, b BigInt) bool {
	if a.Base() != b.Base() {
		return false
	}

	if a.Sign() != b.Sign() {
		return false
	}

	if a.Len() != b.Len() {
		return false
	}

	if !reflect.DeepEqual(a.digits, b.digits) {
		return false
	}

	return true
}

func (a *BigInt) Equal(b BigInt) bool {
	return Equal(*a, b)
}

func FromString(s string, base Base) (BigInt, error) {
	ss, err := MakeIntStr(s, base)
	if err != nil {
		return BigInt{}, err
	}

	return Make(ss.Digits(10), base), nil
}

func processResultDigits(d Digits, base Base) Digits {
	return d.normalizeSigns(10).carryOverflows(10).trimLeadingZeroes()
}

func add(digitsA Digits, digitsB Digits, base Base) Digits {
	// Lattice addition
	max := maxDigits(digitsA, digitsB)
	padA := digitsA.zeroPad(int(max))
	padB := digitsB.zeroPad(int(max))

	// Working from right to left, calculate the sum of each digit from
	// a and b, then set the corresponding entry in sumDigits to that value
	// Add an extra digit for overflow cases
	sumDigits := make(Digits, max+1)
	for i := 0; i < len(padA); i++ {
		sumDigits[i+1] = padA[i] + padB[i]
	}

	return processResultDigits(sumDigits, base)
}

func Add(a BigInt, b BigInt) BigInt {
	return BigInt{digits: add(a.digits, b.digits, a.base), base: a.base}
}

func (a *BigInt) Add(b BigInt) BigInt {
	return Add(*a, b)
}

func sub(digitsA Digits, digitsB Digits, base Base) Digits {
	// zero-pad each list of digits to match the longer of the two
	max := maxDigits(digitsA, digitsB)
	padA := digitsA.zeroPad(int(max))
	padB := digitsB.zeroPad(int(max))

	// Working from right to left, calculate the difference of each digit from
	// a and b, then set the corresponding entry in diffDigits to that value
	// Add an extra digit for overflow cases
	diffDigits := make(Digits, max+1)
	for i := 0; i < len(padA); i++ {
		diffDigits[i+1] = padA[i] - padB[i]
	}

	return processResultDigits(diffDigits, base)
}

func Sub(a BigInt, b BigInt) BigInt {
	return BigInt{digits: sub(a.digits, b.digits, a.base), base: a.base}
}

func (a *BigInt) Sub(b BigInt) BigInt {
	return Sub(*a, b)
}

func mult(digitsA Digits, digitsB Digits, base Base) Digits {
	// http://code.activestate.com/recipes/577782-lattice-multiplication/
	// https://en.wikipedia.org/wiki/Multiplication_algorithm#Lattice_multiplication

	// Calculate the entries of the lattice
	digitProducts := make(Digits, len(digitsA)+len(digitsB))
	for indexA, digitA := range digitsA {
		for indexB, digitB := range digitsB {
			if absDigit(digitA) > base || absDigit(digitB) > base {
				panic("")
			}
			product := digitA * digitB
			// Add the number of "tens" in the digit
			digitProducts[indexA+indexB+0] += product / base
			// Add the number of "ones" in the digit
			digitProducts[indexA+indexB+1] += product % base
		}
	}

	// Sum the diagonals/products
	newDigits := Digits{}
	carry := Digit(0)
	// Moving from right to left in the diagonals
	for i := len(digitProducts) - 1; i >= 0; i-- {
		// Get the current product
		// Add the carry from last iteration
		cur := digitProducts[i] + carry

		// If the current product is > base
		if cur >= base {
			// Set carry to the number of bases in the product
			carry = cur / base
			// Set the new digit to the remainder
			newDigits = append(newDigits, cur%base)
		} else {
			// If the current product is <= base: reset the carry
			carry = 0
			// Set the new digit to the product
			newDigits = append(newDigits, cur)
		}
	}

	// Account for the final carry
	if carry > 0 {
		newDigits = append(newDigits, carry)
	}

	return newDigits.reversed().trimLeadingZeroes()
}

func Mult(a BigInt, b BigInt) BigInt {
	return Make(mult(a.digits, b.digits, a.base), a.base)
}

func (a *BigInt) Mult(b BigInt) BigInt {
	return Mult(*a, b)
}

func (a *BigInt) String() string {
	return a.digits.String(10)
}
