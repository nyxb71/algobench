package sort

import (
	"algobench/common"
	"algobench/data"
	"sort"
	"testing"
)

func TestBogoSort(t *testing.T) {
	list := data.RandomList(4)
	sorted := BogoSort(list)
	if !sort.IntsAreSorted(sorted) {
		t.Error(common.ErrorListNotSorted)
	}
}
