package fib

import (
	"algobench/common"
	"fmt"
	"testing"
)

func TestFibLower(t *testing.T) {
	// For each implementation, run each test case
	implementations := []func(uint64) uint64{FibLoop, FibLoopDP, FibRecur, FibRecurDP, FibMatrix, FibFormula}
	for _, f := range implementations {
		var fibCases = []struct {
			input uint64
			want  uint64
		}{
			// Input array, expected value
			{input: 0, want: 0},
			{input: 1, want: 1},
			{input: 2, want: 1},
			{input: 3, want: 2},
			{input: 4, want: 3},
			{input: 10, want: 55},
			{input: 20, want: 6765},
			{input: 21, want: 10946},
			{input: 31, want: 1346269},
			{input: 32, want: 2178309},
			{input: 33, want: 3524578},
		}

		// Test each case
		for _, elem := range fibCases {
			implName := common.GetFunctionName(f)
			errorText := fmt.Sprintf("Case: %s %v = %d", implName, elem.input, elem.want)
			t.Run(errorText,
				func(t *testing.T) {
					fibOutput := f(elem.input)
					if fibOutput != elem.want {
						t.Errorf(
							"Wrong output for %s(%d), wanted: %d, got: %d\n",
							implName, elem.input, elem.want, fibOutput)
					}
				})
		}
	}
}

func TestFibLimits(t *testing.T) {
	// For each implementation, run each test case
	implementations := []func(uint64) uint64{FibLoop, FibLoopDP, FibRecurDP, FibMatrix}
	for _, f := range implementations {
		var fibCases = []struct {
			input uint64
			want  uint64
		}{
			{input: 93, want: 12200160415121876738},
		}

		// Test each case
		for _, elem := range fibCases {
			implName := common.GetFunctionName(f)
			errorText := fmt.Sprintf("Case: %s %v = %d", implName, elem.input, elem.want)
			t.Run(errorText,
				func(t *testing.T) {
					fibOutput := f(elem.input)
					if fibOutput != elem.want {
						t.Errorf(
							"Wrong output for %s(%d), wanted: %d, got: %d\n",
							implName, elem.input, elem.want, fibOutput)
					}
				})
		}
	}
}

func TestFibFormulaCompare(t *testing.T) {
	for i := uint64(0); i <= 75; i++ {
		want := FibLoop(i)
		got := FibFormula(i)
		if got != want {
			t.Errorf("Wrong output for %d, wanted: %d, got: %d\n",
				i, want, got)
		}
	}
}
