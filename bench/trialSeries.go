package bench

import (
	"algobench/common"
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"github.com/dustin/go-humanize"
)

// TrialSeries represents benchmark metadata for a function
// across a series of input sizes.
type TrialSeries struct {
	Title  string
	Trials []Trial
}

// MakeTrialSeries constructs a TrialSeries
func MakeTrialSeries(
	title string,
	f TimerFunc,
	numTrials uint64,
	inputSteps []uint64) TrialSeries {

	// Allocate array of trials
	trials := make([]Trial, len(inputSteps))
	for index := range trials {
		// Initialize all the trials
		trials[index] = MakeTrial(f, numTrials, inputSteps[index])
	}
	res := TrialSeries{Title: title, Trials: trials}
	return res
}

// Run a series of trials
func (ts *TrialSeries) Run(print bool) {
	if print {
		fmt.Printf("%s (Average of %d trials)\n", ts.Title, ts.Trials[0].NumTrials)
		fmt.Printf("%20s %21s %21s %10s\n", "INPUT", "AVERAGE TIME", "TOTAL TIME", "RATIO")
	}

	// Run each trial, and print each result after it is calculated
	for i := 0; i < len(ts.Trials); i++ {
		tr := &ts.Trials[i]
		tr.Run()
		// Calculate runtime ratios
		if i > 0 {
			ts.Trials[i].RatioToPrevious = float64(
				ts.Trials[i].AverageTime) / float64(ts.Trials[i-1].AverageTime)
		}
		if print {
			fmt.Printf("%20s %18s ns %18s ns %10.3f\n",
				humanize.Comma(int64(tr.InputSize)),
				humanize.Comma(tr.AverageTime.Nanoseconds()),
				humanize.Comma(tr.TotalTime.Nanoseconds()),
				tr.RatioToPrevious)
		}
	}

	if print {
		fmt.Println()
	}
}

// Export the results of a TrialSeries to a file
func (ts *TrialSeries) Export(outputPath string) {
	abspath, err := filepath.Abs(outputPath)
	if err != nil {
		log.Fatalf("Could not find absolute path for: %s\n", outputPath)
	}

	// if the user specifies a filename: use it
	// else: use a default filename
	var outputFilePath string
	if strings.HasSuffix(outputPath, "/") {
		now := time.Now()
		year, month, day := now.Date()
		hour, minute, second := now.Clock()

		timestamp := fmt.Sprintf("%d%02d%02d-%02d%02d%02d",
			year, month, day, hour, minute, second)
		defaultFilename := fmt.Sprintf("%s-%s.csv",
			timestamp, strings.ReplaceAll(ts.Title, " ", ""))

		outputFilePath = path.Join(abspath, defaultFilename)
	} else {
		outputFilePath = abspath
	}

	err = os.MkdirAll(path.Dir(outputFilePath), os.ModePerm)
	if err != nil {
		log.Fatalf("Was not able to create folder: %s\n", path.Dir(outputFilePath))
	}

	file, err := os.Create(outputFilePath)
	if err != nil {
		log.Fatalf("Could not create export file: %s\n", outputFilePath)
	}

	writer := csv.NewWriter(file)
	defer writer.Flush()

	// Write the header, column names
	err = writer.Write([]string{"Input", "InputBits", "AverageTime", "TotalTime", "Ratio", "NumTrials"})
	if err != nil {
		log.Fatalf("Was not able to write trial result to file: %s\n", outputFilePath)
	}

	// Write the data
	for _, elem := range ts.Trials {
		err := writer.Write([]string{
			fmt.Sprintf("%d", elem.InputSize),
			fmt.Sprintf("%d", common.BitsToRepresent(elem.InputSize)),
			fmt.Sprintf("%d", elem.AverageTime.Nanoseconds()),
			fmt.Sprintf("%d", elem.TotalTime.Nanoseconds()),
			fmt.Sprintf("%.3f", elem.RatioToPrevious),
			fmt.Sprintf("%d", elem.NumTrials),
		})
		if err != nil {
			log.Fatal("Was not able to write trial result to file: " + outputFilePath)
		}
	}
}
