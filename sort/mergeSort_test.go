package sort

import (
	"algobench/common"
	"algobench/data"
	"sort"
	"testing"
)

func TestMergeRecursiveSimple(t *testing.T) {
	a := []int{1, 3, 5}
	b := []int{2, 4}
	valid := []int{1, 2, 3, 4, 5}
	result := mergeRecursive(a, b)
	if !common.SlicesEqual(valid, result) {
		t.Errorf(common.ErrorListsNotMergedExpected, valid, result)
	}
}

func TestMergeRecursiveAscendingRandom(t *testing.T) {
	a := data.AscendingList(common.TestListLength / 2)
	b := data.AscendingList(common.TestListLength / 2)
	result := mergeRecursive(a, b)
	if !sort.IntsAreSorted(result) {
		t.Error(common.ErrorListsNotMerged)
	}
}

func TestMergeIterativeSimple(t *testing.T) {
	a := []int{1, 3, 5}
	b := []int{2, 4}
	valid := []int{1, 2, 3, 4, 5}
	result := mergeIterative(a, b)
	if !common.SlicesEqual(valid, result) {
		t.Errorf(common.ErrorListsNotMergedExpected, valid, result)
	}
}

func TestMergeIterativeAscendingRandom(t *testing.T) {
	a := data.AscendingList(common.TestListLength / 2)
	b := data.AscendingList(common.TestListLength / 2)
	result := mergeIterative(a, b)
	if !sort.IntsAreSorted(result) {
		t.Error(common.ErrorListsNotMerged)
	}
}

func TestMergeSortRecursiveSimple(t *testing.T) {
	list := []int{1, 5, 4, 2, 3}
	sorted := MergeSortRecursive(list)
	if !sort.IntsAreSorted(sorted) {
		t.Error(common.ErrorListNotSorted)
	}
}

func TestMergeSortRecursiveRandom(t *testing.T) {
	list := data.RandomList(common.TestListLength)
	sorted := MergeSortRecursive(list)
	if !sort.IntsAreSorted(sorted) {
		t.Error(common.ErrorListNotSorted)
	}
}

func TestMergeSortIterativeSimple(t *testing.T) {
	list := []int{1, 5, 4, 2, 3}
	sorted := MergeSortIterative(list)
	if !sort.IntsAreSorted(sorted) {
		t.Error(common.ErrorListNotSorted)
	}
}

func TestMergeSortIterativeRandom(t *testing.T) {
	list := data.RandomList(common.TestListLength)
	sorted := MergeSortIterative(list)
	if !sort.IntsAreSorted(sorted) {
		t.Error(common.ErrorListNotSorted)
	}
}

func BenchmarkMergeSortIterative(b *testing.B) {
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		list := data.RandomList(1000)
		b.StartTimer()
		MergeSortIterative(list)
		b.StopTimer()
	}
}

func BenchmarkMergeSortRecursive(b *testing.B) {
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		list := data.RandomList(1000)
		b.StartTimer()
		MergeSortRecursive(list)
		b.StopTimer()
	}
}
