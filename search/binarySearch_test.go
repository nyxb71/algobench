package search

import (
	"algobench/common"
	"algobench/data"
	"testing"
)

func TestBinarySearchSimpleExistingElements(t *testing.T) {
	simpleList := []int{1, 2, 3, 4, 5}
	for elemIndex, elem := range simpleList {
		t.Run("Case: "+string(elem), func(t *testing.T) {
			resultIndex, err := BinarySearch(simpleList, elem)
			if err != nil {
				t.Errorf(common.ErrorTargetNotFound, elem)
				t.Fail()
			}
			if resultIndex != elemIndex {
				t.Errorf(common.ErrorWrongResultIndex, elemIndex, resultIndex)
				t.Fail()
			}
		})
	}
}

func TestBinarySearchRandomElement(t *testing.T) {
	list := data.AscendingList(common.TestListLength)

	target, targetIndex := data.GetRandomElementAndIndex(list)
	resultIndex, err := BinarySearch(list, target)
	if err != nil {
		t.Errorf(common.ErrorTargetNotFound, target)
	}
	if resultIndex != int(targetIndex) {
		t.Errorf(common.ErrorWrongResultIndex, targetIndex, resultIndex)
	}
}

func TestBinarySearchNonexistentElement(t *testing.T) {
	for tests := 0; tests < 1000; tests++ {
		list := data.AscendingList(10)

		target, err := common.FindGapInList(list)
		if err != nil {
			t.Error(err)
			break
		}

		if common.Contains(list, target) {
			t.Error("target was in list when it should not be")
			break
		}

		resultIndex, err := BinarySearch(list, target)
		if err == nil || resultIndex != -1 {
			t.Errorf(
				"While searching for element not in list:"+
					" expected index -1, got %d\n", resultIndex)
			break
		}
	}
}
