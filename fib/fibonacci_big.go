package fib

import (
	"algobench/bigmatrix"
	"algobench/common"
	"math/big"
	"strings"
)

func FibLoopBig(x uint64) big.Int {
	a := big.NewInt(0)
	b := big.NewInt(1)

	for i := uint64(0); i < x; i++ {
		a.Add(a, b)
		a, b = b, a
	}

	return *a
}

type matrixPowerCacheBig map[uint64]bigmatrix.Matrix

func fibMatrixBig(cache matrixPowerCacheBig, exponent uint64) bigmatrix.Matrix {
	// If the result is in the cache, return it
	if cachedResult, ok := cache[exponent]; ok {
		return cachedResult
	}

	// Break the exponent into two pieces
	// 7 -> (4,3)
	// 4 -> (2,2)
	// 3 -> (2,1)
	// 1 -> (1,0)
	// 0 -> (0,0)
	expA, expB := common.SplitExponent(exponent)

	// Calculate FibM^expA * FibM*expB
	res, _ := bigmatrix.Mult2x2(
		fibMatrixBig(cache, expA),
		fibMatrixBig(cache, expB))

	// Store the result in the cache, then return it
	cache[exponent] = res
	return cache[exponent]
}

// FibMatrixBig returns the Nth Fibonacci number
// via recursive matrix multiplication and caching
func FibMatrixBig(x uint64) big.Int {
	if x <= 1 {
		return *big.NewInt(int64(x))
	}

	// Populate the cache
	FibM, _ := bigmatrix.From2DArray([][]uint64{{1, 1}, {1, 0}})
	FibM2, _ := bigmatrix.Mult2x2(FibM, FibM)
	cache := matrixPowerCacheBig{
		1: FibM,
		2: FibM2,
	}

	// Calculate FibM^(x-1)
	res := fibMatrixBig(cache, x-1)
	// Get the top-left element, which is Fib(x)
	return res.Cell(0, 0)
}

// big.Float precision
// max 2^32 - 1
const (
	floatPrec = uint(1) << 16
)

func pow(a *big.Float, e uint64) *big.Float {
	base := new(big.Float).SetPrec(floatPrec).Set(a)
	result := new(big.Float).SetPrec(floatPrec).Set(a)
	for i := uint64(0); i < e-1; i++ {
		result.Mul(result, base)
	}
	return result
}

func FibFormulaBig(x uint64) big.Int {
	// https://artofproblemsolving.com/wiki/index.php?title=Binet%27s_Formula
	// https://bosker.wordpress.com/2011/07/27/computing-fibonacci-numbers-using-binet%E2%80%99s-formula/
	// https://stackoverflow.com/a/7843192

	if x <= 1 {
		return *big.NewInt(int64(x))
	}

	// sqrt(5) = 2.236067977499789696409173668731276235440618359611525724270897245...
	sqrt5 := new(big.Float).SetPrec(floatPrec).SetMode(big.ToNearestAway).SetUint64(5)
	sqrt5.Sqrt(sqrt5)

	// 2.0...
	two := new(big.Float).SetPrec(floatPrec).SetMode(big.ToNearestAway).SetUint64(2)

	// (1 + sqrt(5)) / 2
	a := new(big.Float).SetPrec(floatPrec).SetMode(big.ToNearestAway).SetUint64(1)
	// 3.236067977499789696409173668731276235440618359611525724270897245
	a.Add(a, sqrt5)
	// 1.618033988749894848204586834365638117720309179805762862135448623
	a.Quo(a, two)

	// ((1 + sqrt(5)) / 2)^x
	aToX := pow(a, x)

	// (1 - sqrt(5)) / 2
	b := new(big.Float).SetPrec(floatPrec).SetMode(big.ToNearestAway).SetUint64(1)
	b.Sub(b, sqrt5)
	b.Quo(b, two)

	// ((1 - sqrt(5)) / 2)^x
	bToX := pow(b, x)

	// 1 / sqrt(5)
	c := new(big.Float).SetPrec(floatPrec).SetMode(big.ToNearestAway).SetUint64(1)
	c.Quo(c, sqrt5)

	// ((1 + sqrt(5)) / 2)^x - ((1 - sqrt(5)) / 2)^x
	res := big.NewFloat(0).Set(aToX)
	res.Sub(aToX, bToX)

	// (1 / sqrt(5)) * (((1 + sqrt(5)) / 2)^x - ((1 - sqrt(5)) / 2)^x)
	res.Mul(res, c)

	// Get everything before the decimal
	s := strings.Split(res.Text('f', int(floatPrec)), ".")[0]
	// Return that integer as a big.Int
	fibn, _ := new(big.Int).SetString(s, 10)
	return *fibn
}
