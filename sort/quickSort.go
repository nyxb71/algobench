package sort

import (
	"sort"
)

func median3(list []int) int {
	// If the list is less than three numbers, there is no "median",
	// so we return an arbitrary one
	if len(list) < 3 {
		return 0
	}

	firstIndex, middleIndex, lastIndex := 0, len(list)-1, len(list)/2
	l := []int{list[firstIndex], list[middleIndex], list[lastIndex]}
	sort.Ints(l)
	median := l[1]
	if median == list[firstIndex] {
		return firstIndex
	}
	if median == list[lastIndex] {
		return lastIndex
	}
	return middleIndex
}

func quicksort(list []int, naive bool) []int {
	// Based on:
	// https://stackoverflow.com/questions/15802890/idiomatic-quicksort-in-go
	// https://cs.stackexchange.com/a/92070
	// https://yourbasic.org/golang/quicksort-optimizations/
	// https://www.youtube.com/watch?v=ZHVk2blR45Q
	// https://www.youtube.com/watch?v=4IE3wIXFVPc
	// Go slices are pass by reference, so this function modifies the original list

	// Base case
	// If the list we are working on is of length 1 or 0, it is sorted
	if len(list) < 2 {
		return list
	}

	// Set bounds
	lastIndex := len(list) - 1
	low := 0

	var pivotIndex int
	if naive {
		// Just use the first element in the list
		// Degenerate case on sorted lists
		pivotIndex = 0
	} else {
		// Use the median of the first, middle, and last elements
		// Takes more time but has better performance on a variety of lists
		pivotIndex = median3(list)
	}

	// Swap the pivot and the last element
	list[pivotIndex], list[lastIndex] = list[lastIndex], list[pivotIndex]

	// Move all elements less than list[lastIndex] (the pivot) to the left
	for currentIndex := range list {
		// If an element is less than list[lastIndex]
		// swap them and increment the low index
		// (the last thing greater than the pivot)
		if list[currentIndex] < list[lastIndex] {
			list[low], list[currentIndex] = list[currentIndex], list[low]
			low++
		}
	}

	// After all elements of the list < list[lastIndex]
	// have been moved to the left, swap the pivot back
	list[low], list[lastIndex] = list[lastIndex], list[low]

	// Sort the lower half
	quicksort(list[:low], naive)
	// Sort the upper half, excluding the pivot
	quicksort(list[low+1:], naive)

	return list
}

// QuickSort sorts a list
func QuickSort(list []int) []int {
	new := make([]int, len(list))
	copy(new, list)
	quicksort(new, false)
	return new
}

// QuickSortNaive sorts a list, degenerates with pre-sorted lists
func QuickSortNaive(list []int) []int {
	new := make([]int, len(list))
	copy(new, list)
	quicksort(new, true)
	return new
}
