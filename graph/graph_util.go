package graph

import (
	"math"
	"math/rand"
	"time"
)

func RandFloat64(min float64, max float64) float64 {
	// https://stackoverflow.com/a/49747128
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	return min + r.Float64()*(max-min)
}

func RandAngle() angle {
	return angle(RandFloat64(0, 2*math.Pi))
}
