package sort

import (
	"algobench/common"
	"algobench/data"
	"sort"
	"testing"
)

func TestGnomeSort(t *testing.T) {
	list := data.RandomList(common.TestListLength)
	GnomeSort(list)
	if !sort.IntsAreSorted(list) {
		t.Error(common.ErrorListNotSorted)
	}
}
