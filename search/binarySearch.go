package search

import "errors"

func _binarySearch(
	list []int, target int, lowIndex int, highIndex int) (int, error) {

	if len(list) == 0 {
		return -1, errors.New("can't search an empty list")
	}

	// Base case, if we have already searched everything
	if highIndex < lowIndex {
		return -1, errors.New("target not found")
	}

	// Create a new partition point
	middleIndex := (lowIndex + highIndex) / 2

	// Search the lower partition
	if list[middleIndex] > target {
		return _binarySearch(list, target, lowIndex, middleIndex-1)
	}

	// Search the upper partition
	if list[middleIndex] < target {
		return _binarySearch(list, target, middleIndex+1, highIndex)
	}

	return middleIndex, nil
}

// BinarySearch searches a list for the target,
// returns the index if found, and an error if not found
func BinarySearch(list []int, target int) (int, error) {
	return _binarySearch(list, target, 0, len(list)-1)
}
