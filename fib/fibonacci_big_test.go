package fib

import (
	"algobench/common"
	"fmt"
	"math/big"
	"testing"
)

func TestFibBig(t *testing.T) {
	// For each implementation, run each test case
	// Add FibFormulaBig to see where it fails (78 for most precisions)
	implementations := []func(uint64) big.Int{FibLoopBig, FibMatrixBig, FibFormulaBig}
	for _, f := range implementations {
		var cases = []struct {
			input uint64
			want  string
		}{
			{input: 0, want: "0"},
			{input: 1, want: "1"},
			{input: 2, want: "1"},
			{input: 3, want: "2"},
			{input: 4, want: "3"},
			{input: 5, want: "5"},
			{input: 32, want: "2178309"},
			{input: 64, want: "10610209857723"},
			{input: 72, want: "498454011879264"},
			{input: 73, want: "806515533049393"},
			{input: 74, want: "1304969544928657"},
			{input: 75, want: "2111485077978050"},
			{input: 76, want: "3416454622906707"},
			{input: 77, want: "5527939700884757"},
			{input: 78, want: "8944394323791464"},
			{input: 79, want: "14472334024676221"},
			{input: 93, want: "12200160415121876738"},
			{input: 94, want: "19740274219868223167"},
			{input: 128, want: "251728825683549488150424261"},
			{input: 256, want: "141693817714056513234709965875411919657707794958199867"},
		}

		// Test each case
		for _, tc := range cases {
			implName := common.GetFunctionName(f)
			errorText := fmt.Sprintf("Case: %s %v = %s", implName, tc.input, tc.want)
			t.Run(errorText,
				func(t *testing.T) {
					fibOutput := f(tc.input)
					if fibOutput.String() != tc.want {
						t.Errorf(
							"Wrong output for %s(%d)\nwanted: %s\n   got: %s\n",
							implName, tc.input, tc.want, fibOutput.String())
					}
				})
		}
	}
}

// func TestBigPow(t *testing.T) {
// 	var cases = []struct {
// 		input *big.Float
// 		power uint64
// 		want  *big.Float
// 	}{
// 		{input: big.NewFloat(2.0), power: 2, want: big.NewFloat(4.0)},
// 	}

// 	for _, tc := range cases {
// 		t.Run("",
// 			func(t *testing.T) {
// 				result := Pow(tc.input, tc.power)
// 				margin := big.NewFloat(float64(0.000001))
// 				diff := new(big.Float).SetPrec(256).Set(result)
// 				diff.Sub(result, tc.want)
// 				if diff > margin {
// 					t.Errorf(
// 						"Wrong output for %s(%d), wanted: %d, got: %d\n",
// 						implName, elem.input, elem.want, fibOutput)
// 				}
// 			})
// 	}
// }
