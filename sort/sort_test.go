package sort

import (
	"algobench/common"
	"algobench/data"
	"fmt"
	"sort"
	"testing"
)

type SortFunc func([]int) []int

type SortTestCase struct {
	input []int
}

func TestAllSorts(t *testing.T) {
	sorts := []SortFunc{BubbleSort, MergeSortIterative, BogoSort, GnomeSort, QuickSort}

	// For each sorting algorith, run each test case
	for _, f := range sorts {
		// Reset cases for each algorithm
		sortCases := []SortTestCase{
			{input: []int{0}},
			{input: []int{1, 0}},
			{input: []int{2, 1, 0}},
			{input: []int{-2, -1, 0}},
			{input: []int{1, 1, 1, 1, 1, 1, 2, 1}},
			{input: []int{9, 8, 2, 4, 5, 6, 1, 3}},
		}
		for _, testCase := range sortCases {
			algoName := common.GetFunctionName(f)
			t.Run(fmt.Sprintf(
				"Case: %s %v", algoName, testCase.input),
				func(t *testing.T) {
					new := f(testCase.input)
					if !sort.IntsAreSorted(new) {
						t.Errorf("List was not sorted\n")
					}
				})
		}
	}
}

func BenchmarkGoSort(b *testing.B) {
	// combination of quicksort and heapsort
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		list := data.RandomList(1000)
		b.StartTimer()
		sort.Ints(list)
		b.StopTimer()
	}
}
