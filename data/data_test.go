package data

import (
	"algobench/common"
	"fmt"
	"testing"
)

func TestRandomList(t *testing.T) {
	list := RandomList(common.TestListLength)
	if uint(len(list)) != common.TestListLength {
		t.Errorf(common.ErrorListWrongSize, len(list), common.TestListLength)
	}
}

func TestAscendingList(t *testing.T) {
	list := AscendingList(common.TestListLength)
	if uint(len(list)) != common.TestListLength {
		t.Errorf(common.ErrorListWrongSize, len(list), common.TestListLength)
	}

	for i := 1; i < len(list); i++ {
		if list[i] <= list[i-1] {
			t.Errorf("List not ascending: expected > %d, got %d",
				list[i-1], list[i])
		}
	}
}

func TestRandomListWithNegatives(t *testing.T) {
	list := RandomListWithNegatives(common.TestListLength)
	if uint(len(list)) != common.TestListLength {
		t.Errorf(common.ErrorListWrongSize, len(list), common.TestListLength)
	}
}

func TestSeriesMToN(t *testing.T) {
	var cases = []struct {
		m    uint64
		n    uint64
		want []uint64
	}{
		// Input array, expected value
		{m: 2, n: 6, want: []uint64{2, 3, 4, 5, 6}},
		{m: 2, n: 2, want: []uint64{2}},
		{m: 6, n: 2, want: []uint64{6, 5, 4, 3, 2}},
	}

	// Test each case
	for _, tc := range cases {
		t.Run(fmt.Sprintf("%v", tc),
			func(t *testing.T) {
				series := SeriesMToN(tc.m, tc.n)
				if len(series) != len(tc.want) {
					t.Errorf("Wrong length for [%d,%d], wanted: %v, got: %v\n",
						tc.m, tc.n, tc.want, series)
				}
				for i := 0; i < len(series); i++ {
					if series[i] != tc.want[i] {
						t.Errorf(
							"Wrong output for [%d,%d], wanted: %v, got: %b\n",
							tc.m, tc.n, tc.want, series)
					}
				}
			})
	}
}
