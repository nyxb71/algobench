package bigmatrix

import (
	"errors"
	"fmt"
	"log"
	"math/big"
)

// Matrix is a matrix of big.Int
type Matrix struct {
	rows  int
	cols  int
	cells [][]*big.Int
}

// Make creates and initializes a Matrix
func Make(rows int, cols int) (Matrix, error) {
	if rows <= 0 {
		return Matrix{}, errors.New("not enough rows to construct array")
	}
	if cols <= 0 {
		return Matrix{}, errors.New("not enough columns to construct array")
	}

	// // Initialize the matrix
	cells := make([][]*big.Int, rows)
	for i := 0; i < rows; i++ {
		cells[i] = make([]*big.Int, cols)
		for j := 0; j < cols; j++ {
			cells[i][j] = big.NewInt(0)
		}
	}

	return Matrix{rows: rows, cols: cols, cells: cells}, nil
}

// From2DArray creates a matrix from a 2D array
func From2DArray(arr [][]uint64) (Matrix, error) {
	if len(arr) == 0 {
		return Matrix{}, errors.New("not enough rows to construct array")
	}
	if len(arr[0]) == 0 {
		return Matrix{}, errors.New("not enough columns to construct array")
	}

	rows := len(arr)
	cols := len(arr[0])
	new, err := Make(rows, cols)
	if err != nil {
		log.Fatalf("")
	}

	for i := 0; i < rows; i++ {
		for j := 0; j < cols; j++ {
			new.cells[i][j].SetUint64(arr[i][j])
		}
	}

	return new, nil
}

func cell(m Matrix, row uint, col uint) big.Int {
	return *m.cells[row][col]
}

// Cell gets the value of the specified cell
func (m *Matrix) Cell(row uint, col uint) big.Int {
	return cell(*m, row, col)
}

// Identity returns a size*size indentity matrix
func Identity(size int) (Matrix, error) {
	if size < 1 {
		return Matrix{}, errors.New("matrix must be at least 1x1")
	}

	// Initialize the new identity matrix
	id, _ := Make(size, size)
	// Set the diagonal elements to 1
	for row := 0; row < id.rows; row++ {
		for col := 0; col < id.cols; col++ {
			if row == col {
				id.cells[row][col] = big.NewInt(1)
			}
		}
	}

	return id, nil

}

// Equal checks two matrices for equality
func Equal(m Matrix, n Matrix) (bool, error) {
	if m.rows != n.rows {
		return false, fmt.Errorf(
			"number of rows not equal: %d != %d", m.rows, m.cols)
	}

	if m.cols != n.cols {
		return false, fmt.Errorf(
			"number of cols not equal: %d != %d", m.cols, m.cols)
	}

	// For each cell in matrix m, if the corresponding cell is different,
	// return false
	for row := 0; row < m.rows; row++ {
		for col := 0; col < n.cols; col++ {
			if m.cells[row][col].String() != n.cells[row][col].String() {
				return false, fmt.Errorf(
					"different value at (%d,%d) | %s != %s",
					row, col, m.cells[row][col].String(), n.cells[row][col].String())
			}
		}
	}

	return true, nil
}

// Mult2x2 multiplies a 2x2 matrix of big.Int
func Mult2x2(m Matrix, n Matrix) (Matrix, error) {
	if m.rows != 2 || m.cols != 2 || n.rows != 2 || n.cols != 2 {
		return Matrix{}, errors.New("input was not a 2x2 matrix")
	}

	product, err := Make(2, 2)
	if err != nil {
		return Matrix{}, err
	}

	// Manually unroll the loops here for better performance
	// Given:
	// a b * e f = i j
	// c d   g h   k l

	// Intermediate storage
	a := big.NewInt(0)
	b := big.NewInt(0)

	// i = (a * e) + (b * g)
	a.Mul(m.cells[0][0], n.cells[0][0])
	b.Mul(m.cells[0][1], n.cells[1][0])
	product.cells[0][0].Add(a, b)

	// j = (a * f) + (b * h)
	a.Mul(m.cells[0][0], n.cells[0][1])
	b.Mul(m.cells[0][1], n.cells[1][1])
	product.cells[0][1].Add(a, b)

	// k = (c * e) + (d * g)
	a.Mul(m.cells[1][0], n.cells[0][0])
	b.Mul(m.cells[1][1], n.cells[1][0])
	product.cells[1][0].Add(a, b)

	// l = (c * f) + (d * h)
	a.Mul(m.cells[1][0], n.cells[0][1])
	b.Mul(m.cells[1][1], n.cells[1][1])
	product.cells[1][1].Add(a, b)

	return product, nil
}
