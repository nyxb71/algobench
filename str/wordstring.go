package str

import (
	"algobench/common"
	"fmt"
	"regexp"
	"strings"
)

// An ascii string that contains only lowercase and space characters.
// Spaces were kept to prevent false matches between concatentated words
// Example: "or, bit" != "orbit".
// Should not have successive spaces.
type WordString string

var charset = "abcdefghijklmopqrstuvwxyz "

func (w WordString) String() string {
	return string(w)
}

func (w WordString) Len() int {
	return len(w)
}

func (w WordString) Contains(s WordString) bool {
	return strings.Contains(string(w), string(s))
}

// Return a random slice of the desired length from w
func (w WordString) RandomSlice(length uint64) (WordString, error) {
	if int(length) > len(w) {
		return "", fmt.Errorf("length must be less than len(s)")
	}

	start := common.RandInt(0, len(w)-int(length))
	end := start + int(length)
	return w[start:end], nil
}

// Generate a random WordString of length n
func RandomWordString(n uint64) WordString {
	chars := make([]rune, n)
	for i := range chars {
		// Avoid generating multiple adjacent spaces
		if i > 0 && chars[i-1] == ' ' {
			chars[i] = rune(charset[common.RandInt(0, len(charset)-1)])
		} else {
			chars[i] = rune(charset[common.RandInt(0, len(charset))])
		}
	}

	return WordString(chars)
}

// Generate a WordString consisting of n copies of r
func RepeatWordString(r rune, n uint64) WordString {
	runes := make([]rune, n)
	for i := range runes {
		runes[i] = r
	}
	return WordString(runes)
}

// Parse a regular string, and if successful return it
func ParseWordString(s string) (WordString, error) {
	if s == "" {
		return WordString(s), nil
	}

	matched, err := regexp.Match(`^([a-z ]*)$`, []byte(s))
	if err != nil {
		return "", err
	}

	if !matched {
		return "", fmt.Errorf("input is not a valid WordString")
	}

	return WordString(s), nil
}

// Convert a normal string to a WordString
func ToWordString(s string) (WordString, error) {
	if s == "" {
		return WordString(""), nil
	}

	lower := strings.ToLower(s)

	// Filter out everything but lowercase letters and spaces
	filtered := []rune{}
	for i := 0; i < len(lower); i++ {
		if (lower[i] >= 'a' && lower[i] <= 'z') || lower[i] == ' ' {
			// Don't include successive spaces
			if i > 0 && lower[i-1] == ' ' && lower[i] == ' ' {
				continue
			}
			filtered = append(filtered, rune(lower[i]))

		}
	}
	// Make sure the filtered string is a valid WordString
	ws, err := ParseWordString(string(filtered))
	if err != nil {
		return "", err
	}

	return ws, nil
}
