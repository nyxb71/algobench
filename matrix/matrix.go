package matrix

import (
	"errors"
	"fmt"
)

// Matrix is a two-dimensional matrix
type Matrix struct {
	rows  int
	cols  int
	cells [][]uint64
}

// Make creates and initializes a Matrix
func Make(rows int, cols int) (Matrix, error) {
	if rows <= 0 {
		return Matrix{}, errors.New("not enough rows to construct array")
	}
	if cols <= 0 {
		return Matrix{}, errors.New("not enough columns to construct array")
	}

	// Initialize the matrix
	cells := make([][]uint64, rows)
	for i := 0; i < rows; i++ {
		cells[i] = make([]uint64, cols)
	}

	return Matrix{rows: rows, cols: cols, cells: cells}, nil
}

func cell(m Matrix, row uint, col uint) uint64 {
	return m.cells[row][col]
}

// Cell returns the value in the specified cell
func (m *Matrix) Cell(row uint, col uint) uint64 {
	return cell(*m, row, col)
}

// From2DArray creates a matrix from a 2D array
func From2DArray(arr [][]uint64) (Matrix, error) {
	if len(arr) == 0 {
		return Matrix{}, errors.New("not enough rows to construct array")
	}
	if len(arr[0]) == 0 {
		return Matrix{}, errors.New("not enough columns to construct array")
	}

	return Matrix{rows: len(arr), cols: len(arr[0]), cells: arr}, nil
}

// Clone creates a copy of the matrix
func (m *Matrix) Clone() Matrix {
	return *m
}

// Equal checks two matrices for equality
func Equal(m Matrix, n Matrix) (bool, error) {
	if m.rows != n.rows {
		return false, fmt.Errorf(
			"number of rows not equal: %d != %d", m.rows, m.cols)
	}

	if m.cols != n.cols {
		return false, fmt.Errorf(
			"number of cols not equal: %d != %d", m.cols, m.cols)
	}

	// For each cell in matrix m, if the corresponding cell is different,
	// return false
	for row := 0; row < m.rows; row++ {
		for col := 0; col < n.cols; col++ {
			if m.cells[row][col] != n.cells[row][col] {
				return false, fmt.Errorf(
					"different value at (%d,%d) | %d != %d",
					row, col, m.cells[row][col], n.cells[row][col])
			}
		}
	}

	return true, nil
}

// IsSquare returns true if the matrix is square
func (m *Matrix) IsSquare() bool {
	return m.rows == m.cols
}

// Add two matricies
func Add(m Matrix, n Matrix) (Matrix, error) {
	if m.rows != n.rows || n.rows != n.cols {
		return Matrix{}, errors.New(
			"matricies must have same dimensions to add")
	}

	// Initialize the new matrix
	sum, err := Make(m.rows, m.cols)
	if err != nil {
		return Matrix{}, err
	}

	// For each cell, add the corresponding cells from m and n
	for row := 0; row < len(m.cells); row++ {
		for col := 0; col < len(m.cells[0]); col++ {
			sum.cells[row][col] = m.cells[row][col] + n.cells[row][col]
		}
	}

	return sum, nil
}

// selectRow gets the specified row from m
func (m *Matrix) selectRow(row int) []uint64 {
	return m.cells[row]
}

// selectCol gets the specified column from m
func (m *Matrix) selectCol(col int) []uint64 {
	colElements := []uint64{}
	for row := 0; row < m.rows; row++ {
		colElements = append(colElements, m.cells[row][col])
	}
	return colElements
}

func dotProduct(a []uint64, b []uint64) (uint64, error) {
	if len(a) != len(b) {
		return 0, errors.New("cannot take dot product, " +
			"vectors are not the same length")
	}

	// multiply the corresponding terms from each vector
	// then add that product to the total sum
	// a[0] * b[0] + a[1] * b[1] + ...
	sum := uint64(0)
	for i := 0; i < len(a); i++ {
		sum += a[i] * b[i]
	}
	return sum, nil
}

// Mult2x2 multiplies two 2x2 matrices
func Mult2x2(m Matrix, n Matrix) (Matrix, error) {
	if m.rows != 2 || m.cols != 2 || n.rows != 2 || n.cols != 2 {
		return Matrix{}, errors.New("input was not a 2x2 matrix")
	}

	product, err := Make(2, 2)
	if err != nil {
		return Matrix{}, err
	}

	// Manually unroll the loops here for better performance
	// Given:
	// a b * e f = i j
	// c d   g h   k l

	// i = (a * e) + (b * g)
	product.cells[0][0] =
		(m.cells[0][0] * n.cells[0][0]) +
			(m.cells[0][1] * n.cells[1][0])

	// j = (a * f) + (b * h)
	product.cells[0][1] =
		(m.cells[0][0] * n.cells[0][1]) +
			(m.cells[0][1] * n.cells[1][1])

	// k = (c * e) + (d * g)
	product.cells[1][0] =
		(m.cells[1][0] * n.cells[0][0]) +
			(m.cells[1][1] * n.cells[1][0])

	// l = (c * f) + (d * h)
	product.cells[1][1] =
		(m.cells[1][0] * n.cells[0][1]) +
			(m.cells[1][1] * n.cells[1][1])

	return product, nil
}

func multGeneral(m Matrix, n Matrix) (Matrix, error) {
	// Initialize the new product matrix
	product, err := Make(m.rows, n.cols)
	if err != nil {
		return Matrix{}, err
	}

	// For each cell, find its new value by taking the dot product
	// of the corresponding rows from m and columns from n
	for row := 0; row < product.rows; row++ {
		for col := 0; col < product.cols; col++ {
			mrow := m.selectRow(row)
			ncol := n.selectCol(col)
			sum, err := dotProduct(mrow, ncol)
			if err != nil {
				return Matrix{}, err
			}
			product.cells[row][col] = sum
		}
	}

	return product, nil
}

// Mult multiplies two matrices
func Mult(m Matrix, n Matrix) (Matrix, error) {
	if m.cols != n.rows {
		return Matrix{}, errors.New(
			"matrix multiplication is undefined for these matricies. " +
				"m.cols != n.rows")
	}

	// Special case for 2x2 matricies
	if m.rows == 2 && m.cols == 2 && n.rows == 2 && n.cols == 2 {
		return Mult2x2(m, n)
	}

	// General case
	return multGeneral(m, n)
}

// Identity returns a size*size indentity matrix
func Identity(size int) (Matrix, error) {
	if size < 1 {
		return Matrix{}, errors.New("matrix must be at least 1x1")
	}

	// Initialize the new identity matrix
	id, _ := Make(size, size)
	// Set the diagonal elements to 1
	for row := 0; row < id.rows; row++ {
		for col := 0; col < id.cols; col++ {
			if row == col {
				id.cells[row][col] = 1
			}
		}
	}

	return id, nil

}
