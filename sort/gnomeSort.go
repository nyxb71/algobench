package sort

func gnomeSort(list []int) {
	// https://en.wikipedia.org/wiki/Gnome_sort

	pos := 0
	for pos < len(list) {
		if pos == 0 || list[pos] >= list[pos-1] {
			pos++
		} else {
			list[pos], list[pos-1] = list[pos-1], list[pos]
			pos--
		}
	}
}

func GnomeSort(list []int) []int {
	new := list
	gnomeSort(new)
	return new
}
