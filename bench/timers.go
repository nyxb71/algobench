package bench

import (
	"algobench/bigint"
	"algobench/data"
	"algobench/fib"
	"algobench/graph"
	"algobench/search"
	"algobench/sort"
	"algobench/str"
	"algobench/threesum"
	"math/big"
	"time"
)

// Here we define functions that set up inputs for functions we want to test,
// then run those functions and report the elapsed time.

// TimerFunc is a function that takes a uint64 (usually input or input size),
// times an algorithm running under that input, and returns the
// time elapsed.
type TimerFunc = func(uint64) time.Duration

// TimerInputGeneratorFunc is a function that produces inputs for TimerFuncs
type TimerInputGeneratorFunc = func(uint64) []int

// Timer is a pairing between a TimerFunc and it's inputs
type Timer struct {
	F          TimerFunc
	InputSteps []uint64
}

type timerMap struct {
	Map map[string]Timer
}

func (t *timerMap) Keys() []string {
	keys := make([]string, len(t.Map))
	for key := range t.Map {
		keys = append(keys, key)
	}
	return keys
}

// -------------------- SEARCH --------------------

func MakeSearchTimer(f func([]int, int) (int, error),
	useRandomInput bool) TimerFunc {
	return func(inputSize uint64) time.Duration {
		var input []int
		if useRandomInput {
			input = data.RandomList(inputSize)
		} else {
			input = data.AscendingList(inputSize)
		}

		target, _ := data.GetRandomElementAndIndex(input)
		timeStart := time.Now()
		f(input, target)

		return time.Since(timeStart)

	}
}

var searchDefaultSteps = data.SeriesExponential(2, 1, 14)

// SearchTimers maps sort algorithm names to Timer structs,
// so the CLI has something to match on
var SearchTimers = timerMap{map[string]Timer{
	"binary": Timer{MakeSearchTimer(search.BinarySearch, true), searchDefaultSteps},
	"linear": Timer{MakeSearchTimer(search.LinearSearch, false), searchDefaultSteps},
}}

// -------------------- SORT --------------------

func MakeSortTimer(f func([]int) []int) TimerFunc {
	return func(inputSize uint64) time.Duration {
		list := data.RandomList(inputSize)
		timeStart := time.Now()
		f(list)
		return time.Since(timeStart)
	}
}

// Define input step sizes depending on how slow each algorithm is.
var sortDefaultStepsNFactorial = data.SeriesExponential(2, 1, 3)
var sortDefaultStepsN2 = data.SeriesExponential(2, 1, 14)
var sortDefaultStepsNLogN = data.SeriesExponential(2, 1, 23)

// SortTimers maps sort algorithm names to Timers
var SortTimers = timerMap{map[string]Timer{
	"bogo":       Timer{MakeSortTimer(sort.BogoSort), sortDefaultStepsNFactorial},
	"gnome":      Timer{MakeSortTimer(sort.GnomeSort), sortDefaultStepsN2},
	"bubble":     Timer{MakeSortTimer(sort.BubbleSort), sortDefaultStepsN2},
	"merge":      Timer{MakeSortTimer(sort.MergeSortIterative), sortDefaultStepsNLogN},
	"quick":      Timer{MakeSortTimer(sort.QuickSort), sortDefaultStepsNLogN},
	"quicknaive": Timer{MakeSortTimer(sort.QuickSortNaive), sortDefaultStepsNLogN},
}}

// -------------------- MISC --------------------

func MakeThreeSumTimer(f func([]int) uint) TimerFunc {
	return func(inputSize uint64) time.Duration {
		list := data.RandomListWithNegatives(inputSize)
		start := time.Now()
		f(list)
		return time.Since(start)
	}
}

func MakeFibTimer(f func(uint64) uint64) TimerFunc {
	return func(x uint64) time.Duration {
		start := time.Now()
		f(x)
		return time.Since(start)
	}
}

func MakeFibTimerBig(f func(uint64) big.Int) TimerFunc {
	return func(x uint64) time.Duration {
		start := time.Now()
		f(x)
		return time.Since(start)
	}
}

func MakeBigIntTimer(f func(bigint.BigInt, bigint.BigInt) bigint.BigInt) TimerFunc {
	return func(n uint64) time.Duration {
		// generate two random numbers of length x
		a := bigint.Make(bigint.RandomDigits(n, 10), 10)
		b := bigint.Make(bigint.RandomDigits(n, 10), 10)
		start := time.Now()
		f(a, b)
		return time.Since(start)
	}
}

var threeSumDefaultSteps = data.SeriesExponential(2, 1, 12)
var fibSteps1To93 = data.SeriesMToN(1, 93)
var fibSteps1To35 = data.SeriesMToN(1, 35)
var fibStepsDoubling14 = data.SeriesExponential(2, 0, 14)
var fibStepsDoubling23 = data.SeriesExponential(2, 0, 21)

// MiscTimers maps threesum/fib algorithm names to Timers
var MiscTimers = timerMap{map[string]Timer{
	"threesum":        Timer{MakeThreeSumTimer(threesum.ThreeSum), threeSumDefaultSteps},
	"threesumfast":    Timer{MakeThreeSumTimer(threesum.ThreeSumFast), threeSumDefaultSteps},
	"threesumfastera": Timer{MakeThreeSumTimer(threesum.ThreeSumFasterA), threeSumDefaultSteps},
	"threesumfasterb": Timer{MakeThreeSumTimer(threesum.ThreeSumFasterB), threeSumDefaultSteps},

	"fibloop":    Timer{MakeFibTimer(fib.FibLoop), fibSteps1To93},
	"fibloopdp":  Timer{MakeFibTimer(fib.FibLoopDP), fibSteps1To93},
	"fibrecur":   Timer{MakeFibTimer(fib.FibRecur), fibSteps1To35},
	"fibrecurdp": Timer{MakeFibTimer(fib.FibRecurDP), fibSteps1To93},
	"fibmatrix":  Timer{MakeFibTimer(fib.FibMatrix), fibSteps1To93},
	"fibformula": Timer{MakeFibTimer(fib.FibFormula), fibSteps1To93},

	"fibloopbig1to93":        Timer{MakeFibTimerBig(fib.FibLoopBig), fibSteps1To93},
	"fibloopbigdoubling":     Timer{MakeFibTimerBig(fib.FibLoopBig), fibStepsDoubling23},
	"fibmatrixbig1to93":      Timer{MakeFibTimerBig(fib.FibMatrixBig), fibSteps1To93},
	"fibmatrixbigdoubling":   Timer{MakeFibTimerBig(fib.FibMatrixBig), fibStepsDoubling14},
	"fibmatrixbigdoubling23": Timer{MakeFibTimerBig(fib.FibMatrixBig), fibStepsDoubling23},
	"fibformulabig1to93":     Timer{MakeFibTimerBig(fib.FibFormulaBig), fibSteps1To93},
	"fibformulabigdoubling":  Timer{MakeFibTimerBig(fib.FibFormulaBig), fibStepsDoubling14},
}}

var bigIntSteps = data.SeriesExponential(2, 0, 14)

var BigIntTimers = timerMap{map[string]Timer{
	"add":  Timer{MakeBigIntTimer(bigint.Add), bigIntSteps},
	"sub":  Timer{MakeBigIntTimer(bigint.Sub), bigIntSteps},
	"mult": Timer{MakeBigIntTimer(bigint.Mult), bigIntSteps},
}}

// -------------------- LCS --------------------

type lcsFunc func(str.WordString, str.WordString) str.WordString

func MakeLCSTimer(f lcsFunc, inputType string) TimerFunc {
	return func(n uint64) time.Duration {
		a := str.WordString("")
		b := str.WordString("")
		switch inputType {
		case "random":
			a = str.RandomWordString(n)
			b = str.RandomWordString(n)
		case "worst":
			a = str.RepeatWordString('a', n)
			b = str.RepeatWordString('a', n)
		case "english":
			a, _ = str.GreatExpectations.RandomSlice(n)
			b, _ = str.TaleOfTwoCities.RandomSlice(n)
		}

		start := time.Now()
		f(a, b)
		return time.Since(start)
	}
}

var stringsSteps10 = data.SeriesExponential(2, 0, 10)
var stringsSteps12 = data.SeriesExponential(2, 0, 12)
var stringsSteps14 = data.SeriesExponential(2, 0, 14)

var LCSTimers = timerMap{map[string]Timer{
	"brute.random":     Timer{MakeLCSTimer(str.LCSBrute, "random"), stringsSteps10},
	"brute.worst":      Timer{MakeLCSTimer(str.LCSBrute, "worst"), stringsSteps10},
	"brute.english":    Timer{MakeLCSTimer(str.LCSBrute, "english"), stringsSteps10},
	"matrix.random":    Timer{MakeLCSTimer(str.LCSMatrix, "random"), stringsSteps14},
	"matrix.worst":     Timer{MakeLCSTimer(str.LCSMatrix, "worst"), stringsSteps14},
	"matrix.english":   Timer{MakeLCSTimer(str.LCSMatrix, "english"), stringsSteps14},
	"fragment.random":  Timer{MakeLCSTimer(str.LCSFragmentCache, "random"), stringsSteps14},
	"fragment.worst":   Timer{MakeLCSTimer(str.LCSFragmentCache, "worst"), stringsSteps10},
	"fragment.english": Timer{MakeLCSTimer(str.LCSFragmentCache, "english"), stringsSteps14},
}}

// -------------------- TSP --------------------

type tspFunc func(*graph.Graph) graph.GraphPath

func MakeTSPTimer(f tspFunc, inputType string) TimerFunc {
	return func(n uint64) time.Duration {
		input := graph.Graph{}

		switch inputType {
		case "random":
			input = graph.MakeRandomGraph(n, 0, 100)
		case "euclidian":
			input = graph.MakeRandomEuclidianGraph(n, 0, 100)
		case "circle":
			input = graph.MakeRandomCirclularGraph(n, 100)
		}

		start := time.Now()
		f(&input)
		return time.Since(start)
	}
}

var tcsStepsShort = data.SeriesMToN(3, 11)
var tcsStepsMedium = data.SeriesMToN(3, 15)
var tcsStepsFast = data.SeriesExponential(2, 1, 11)

var TSPTimers = timerMap{map[string]Timer{
	"brute.random":     Timer{MakeTSPTimer(graph.TSPBrute, "random"), tcsStepsShort},
	"brute.euclidian":  Timer{MakeTSPTimer(graph.TSPBrute, "euclidian"), tcsStepsShort},
	"brute.circle":     Timer{MakeTSPTimer(graph.TSPBrute, "circle"), tcsStepsShort},
	"dp.random":        Timer{MakeTSPTimer(graph.TSPDP, "random"), tcsStepsMedium},
	"dp.euclidian":     Timer{MakeTSPTimer(graph.TSPDP, "euclidian"), tcsStepsMedium},
	"dp.circle":        Timer{MakeTSPTimer(graph.TSPDP, "circle"), tcsStepsMedium},
	"greedy.random":    Timer{MakeTSPTimer(graph.TSPGreedy, "random"), tcsStepsFast},
	"greedy.euclidian": Timer{MakeTSPTimer(graph.TSPGreedy, "euclidian"), tcsStepsFast},
	"greedy.circle":    Timer{MakeTSPTimer(graph.TSPGreedy, "circle"), tcsStepsFast},
}}
