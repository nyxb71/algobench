package bigint

import (
	"algobench/common"
	"fmt"
	"regexp"
	"strings"
)

type IntStr string

func (i IntStr) Digits(base Base) Digits {
	sign := 1
	if i[0] == '-' {
		sign = -1
	}

	trimmed := strings.TrimLeft(string(i), "-")

	digits := make([]Digit, len(trimmed))

	for i := 0; i < len(trimmed); i++ {
		digits[i] = Digit(sign) * absDigit(runeToDigit[rune(trimmed[i])])
	}

	return digits
}

func (i IntStr) String() string {
	return string(i)
}

func MakeIntStr(s string, base Base) (IntStr, error) {
	if base != 10 && base != 16 {
		return "", fmt.Errorf("available bases: 10, 16")
	}

	matched, err := regexp.Match(`^(-?)(\d+)$`, []byte(s))
	if err != nil {
		return IntStr(""), err
	}

	if !matched {
		return IntStr(""), fmt.Errorf("could not parse input: %s", s)
	}

	return IntStr(s), nil
}

func RandomDigits(length uint64, base Base) Digits {
	digits := make(Digits, length)
	for i := uint64(0); i < length; i++ {
		digits[0] = Digit(common.RandInt(0, int(base)))
	}
	return digits
}
