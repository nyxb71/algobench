package common

import (
	"errors"
	"math/rand"
	"reflect"
	"runtime"
	"strings"
	"time"
)

func Contains(list []int, target int) bool {
	for _, elem := range list {
		if elem == target {
			return true
		}
	}
	return false
}

func FindGapInList(list []int) (int, error) {
	for i := 0; i < len(list)-1; i++ {
		if list[i+1]-list[i] > 1 {
			return list[i] + 1, nil
		}
	}

	return 0, errors.New("could not find a gap in ascending list, " +
		"expected at least one gap")
}

func SlicesEqual(a []int, b []int) bool {
	if len(a) != len(b) {
		return false
	}

	for index, elem := range a {
		if b[index] != elem {
			return false
		}
	}

	return true
}

func GetFunctionName(i interface{}) string {
	base := runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
	foo := strings.Split(base, ".")
	return foo[len(foo)-1]
}

func IsSorted(list []int) bool {
	for i := 1; i < len(list); i++ {
		if list[i] < list[i-1] {
			return false
		}
	}
	return true
}

func RandInt(min int, max int) int {
	if min == max {
		return min
	}

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	return r.Intn(max-min) + min
}

// SplitExponent breaks a number into two parts
// 7 -> (4,3)
// 4 -> (2,2)
// 3 -> (2,1)
// 1 -> (1,0)
// 0 -> (0,0)
func SplitExponent(exponent uint64) (uint64, uint64) {
	if exponent <= 1 {
		return exponent, 0
	}

	// If the exponent is even we can just divide it evenly
	if exponent%2 == 0 {
		half := exponent / 2
		return half, half
	}

	// The current total sum
	sum := uint64(0)

	currentVal := exponent
	// Track the number of shifts done, so we can get currentBit
	shifts := uint(0)
	// If currentVal > 0, shift it to the right, and increment the bit count
	for currentVal > 1 {
		// The place/digit for the current bit
		currentBit := uint64(1 << shifts)
		// Is currentBit set to 1 in u?
		currentBitIsPresent := (exponent>>shifts)&0x1 == 0x1
		// If true, add it to the bit field
		if currentBitIsPresent {
			sum += currentBit
		}

		currentVal >>= 1
		shifts++
	}

	// Return the value of the most significant bit,
	// and the sum of the values of the other bits
	return uint64(1 << shifts), sum
}

func BitsToRepresent(x uint64) uint64 {
	if x <= 1 {
		return 1
	}

	bits := uint64(0)
	cur := x
	for cur > 0 {
		cur >>= 1
		bits++
	}
	return bits
}
