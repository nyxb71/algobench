package str

import (
	"io/ioutil"
	"path/filepath"
)

var TaleOfTwoCities WordString
var GreatExpectations WordString

func LoadText(fp string) WordString {
	abspath, err := filepath.Abs(fp)
	if err != nil {
		panic(err)
	}

	text, err := ioutil.ReadFile(abspath)
	if err != nil {
		panic(err)
	}

	ws, err := ToWordString(string(text))
	if err != nil {
		panic(err)
	}

	return ws
}

func init() {
	dir := "str"

	TaleOfTwoCities = LoadText(filepath.Join(dir, "englishTaleTwoCites.txt"))
	GreatExpectations = LoadText(filepath.Join(dir, "englishGreatExpectations.txt"))
}
