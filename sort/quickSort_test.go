package sort

import (
	"algobench/common"
	"algobench/data"
	"sort"
	"testing"
)

func TestQuickSort(t *testing.T) {
	list := data.RandomList(common.TestListLength)
	sorted := QuickSort(list)
	if !sort.IntsAreSorted(sorted) {
		t.Error(common.ErrorListNotSorted)
	}
}
