package search

import "errors"

// LinearSearch forward searches a list for the target,
// returns the index if found, and an error if not found
func LinearSearch(list []int, target int) (int, error) {
	for index, elem := range list {
		if elem == target {
			return index, nil
		}
	}

	return -1, errors.New("element not found")
}
