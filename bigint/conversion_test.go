package bigint

import (
	"fmt"
	"math"
	"reflect"
	"testing"
)

func TestFromString(t *testing.T) {
	var cases = []struct {
		input string
		want  BigInt
	}{
		{input: "0", want: Make(Digits{0}, 10)},
		{input: "1", want: Make(Digits{1}, 10)},
		{input: "-1", want: Make(Digits{-1}, 10)},
		{input: fmt.Sprintf("%d", math.MaxInt64),
			want: BigInt{
				digits: Digits{9, 2, 2, 3, 3, 7, 2, 0, 3, 6, 8, 5, 4, 7, 7, 5, 8, 0, 7},
				base:   10}},
		{input: fmt.Sprintf("%d", math.MinInt64),
			want: BigInt{
				digits: Digits{
					-9, -2, -2, -3, -3, -7, -2, -0, -3, -6, -8, -5, -4, -7, -7, -5, -8, 0, -8},
				base: 10}},
		{input: "340282366920938463463374607431768211456",
			want: BigInt{
				digits: Digits{
					3, 4, 0, 2, 8, 2, 3, 6, 6, 9, 2, 0, 9, 3, 8, 4, 6, 3, 4, 6, 3, 3,
					7, 4, 6, 0, 7, 4, 3, 1, 7, 6, 8, 2, 1, 1, 4, 5, 6},
				base: 10}},
		{input: "-340282366920938463463374607431768211456",
			want: BigInt{
				digits: Digits{
					-3, -4, -0, -2, -8, -2, -3, -6, -6, -9, -2, -0, -9, -3, -8, -4,
					-6, -3, -4, -6, -3, -3, -7, -4, -6, -0, -7, -4, -3, -1, -7, -6,
					-8, -2, -1, -1, -4, -5, -6},
				base: 10}},
	}

	for _, tc := range cases {
		errorText := fmt.Sprintf("Case: '%s' = '%v'", tc.input, tc.want)
		t.Run(errorText,
			func(t *testing.T) {
				bi, err := FromString(tc.input, 10)
				if err != nil {
					t.Error(err)
				}
				if !Equal(bi, tc.want) {
					t.Errorf(
						"Wrong output for %s\nwanted: %v\n   got: %v\n",
						tc.input, tc.want, bi)
				}
			})
	}
}

func TestToString(t *testing.T) {
	var cases = []struct {
		input BigInt
		want  string
	}{
		{input: BigInt{digits: Digits{0}}, want: "0"},
		{input: BigInt{digits: Digits{1}}, want: "1"},
		{input: BigInt{digits: Digits{-1}}, want: "-1"},

		{
			input: BigInt{digits: Digits{
				9, 2, 2, 3, 3, 7, 2, 0, 3, 6, 8, 5, 4, 7, 7, 5, 8, 0, 7}, base: 10},
			want: fmt.Sprintf("%d", math.MaxInt64),
		},

		{
			input: BigInt{digits: Digits{
				-9, -2, -2, -3, -3, -7, -2, -0, -3, -6, -8, -5, -4, -7, -7, -5, -8, 0, -8}, base: 10},
			want: fmt.Sprintf("%d", math.MinInt64),
		},

		{
			input: BigInt{digits: Digits{
				3, 4, 0, 2, 8, 2, 3, 6, 6, 9, 2, 0, 9, 3, 8, 4,
				6, 3, 4, 6, 3, 3, 7, 4, 6, 0, 7, 4, 3, 1, 7, 6,
				8, 2, 1, 1, 4, 5, 6}, base: 10},
			want: "340282366920938463463374607431768211456",
		},
		{
			input: BigInt{digits: Digits{
				-3, -4, -0, -2, -8, -2, -3, -6, -6, -9, -2, -0, -9, -3, -8, -4,
				-6, -3, -4, -6, -3, -3, -7, -4, -6, -0, -7, -4, -3, -1, -7, -6,
				-8, -2, -1, -1, -4, -5, -6}, base: 10},
			want: "-340282366920938463463374607431768211456",
		},
	}

	for _, tc := range cases {
		errorText := fmt.Sprintf("Case: '%v' -> '%s'", tc.input, tc.want)
		t.Run(errorText,
			func(t *testing.T) {
				str := tc.input.String()
				if str != tc.want {
					t.Errorf(
						"Wrong output for %v\nwanted: %s\n   got: %s\n",
						tc.input, tc.want, str)
				}
			})
	}
}

func TestStringRoundTrip(t *testing.T) {
	var cases = []struct {
		input string
	}{
		{input: "0"},
		{input: "1"},
		{input: "-1"},
		{input: "340282366920938463463374607431768211456"},
		{input: "-340282366920938463463374607431768211456"},
	}

	for _, tc := range cases {
		caseText := fmt.Sprintf("Case: '%s'", tc.input)
		t.Run(caseText,
			func(t *testing.T) {
				is, err := MakeIntStr(tc.input, 10)
				if err != nil {
					t.Error(err)
				}
				digits := is.Digits(10)
				if digits.String(10) != tc.input {
					t.Errorf(
						"Wrong output for %s, wanted: %s, got: %s\n",
						tc.input, tc.input, is.String())
				}
			})
	}
}

func TestIntToDigits(t *testing.T) {
	var cases = []struct {
		a    int64
		want Digits
	}{
		{a: 1, want: Digits{1}},
		{a: 0, want: Digits{0}},
		{a: -1, want: Digits{-1}},
		{a: 10, want: Digits{1, 0}},
		{a: -10, want: Digits{-1, 0}},
		{a: 100, want: Digits{1, 0, 0}},
		{a: -100, want: Digits{-1, 0, 0}},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("%v", tc.a),
			func(t *testing.T) {
				digits := FromInt(tc.a, 10)
				if !reflect.DeepEqual(digits, tc.want) {
					t.Errorf(
						"Wrong output for %v, wanted: %v, got: %v\n",
						tc.a, tc.want, digits)
				}
			})
	}
}
