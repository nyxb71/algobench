package str

import "algobench/common"

type fragmentCache map[string][]int

// Insert a key:val pair into the cache
func (cache fragmentCache) insert(key string, val int) {
	// Only insert a value if it isn't already present int the cache

	_, ok := cache[key]
	// If the key is non-empty and the value isn't already stored,
	// append the value to the list
	if ok && !common.Contains(cache[key], val) {
		cache[key] = append(cache[key], val)
	}

	if !ok {
		cache[key] = []int{val}
	}
}

// Get a list of keys in the cache
func (cache fragmentCache) keys() []string {
	keys := make([]string, 0, len(cache))
	for key := range cache {
		keys = append(keys, key)
	}
	return keys
}

// Get the longest key in the cache
func (cache fragmentCache) longestKey() string {
	keys := cache.keys()
	longestKey := keys[0]
	for _, k := range cache.keys() {
		if len(k) > len(longestKey) {
			longestKey = k
		}
	}
	return longestKey
}
