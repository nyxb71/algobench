package graph

import (
	"algobench/common"
	"fmt"
	"testing"
)

func TestPerm(t *testing.T) {
	perms := NewPermutationChan([]int{1, 2, 3, 4})

	for p := range perms {
		fmt.Printf("%v\n", p)
	}
}

func TestPathCost(t *testing.T) {
	g := MakeRandomEuclidianGraph(4, 0, 10)

	c := g.PathCost([]int{0, 1, 2, 3})
	fmt.Printf("%.3f\n", c)
}

func PrintResults(f func(*Graph) GraphPath, n uint64, r radius) {
	g := MakeRandomCirclularGraph(n, r)
	gp := f(&g)

	fn := common.GetFunctionName(f)

	fmt.Printf("%s, n: %d, r: %.3f\n", fn, n, r)
	g.printNodes()
	g.printCosts()
	gp.Print(&g)
}

func TestTSPBruteCirc(t *testing.T) {
	PrintResults(TSPBrute, 8, 100)
}

func TestTSPGreedyCirc(t *testing.T) {
	PrintResults(TSPGreedy, 5, 100)
}

func TestTSPDP(t *testing.T) {
	g := MakeRandomCirclularGraph(4, 1)
	gp := TSPDP(&g)
	fmt.Printf("TSPDP: %v %.3f\n", gp.path, gp.c)
}

func TestSQR(t *testing.T) {
	for inputSize := uint64(3); inputSize <= 11; inputSize++ {
		sqrTrails := []cost{}
		for samples := 0; samples < 30; samples++ {
			g := MakeRandomEuclidianGraph(inputSize, 0, 100)
			h := g
			bgp := TSPBrute(&g)
			ggp := TSPGreedy(&h)
			sqr := bgp.c / ggp.c
			sqrTrails = append(sqrTrails, sqr)
		}
		sqrAvg := cost(0)
		for _, v := range sqrTrails {
			sqrAvg += v
		}
		sqrAvg /= cost(len(sqrTrails))

		fmt.Printf("%d,%.3f\n", inputSize, sqrAvg)
	}

}

func TestPackNodeSet(t *testing.T) {
	var cases = []struct {
		input NodeSet
		want  PackedNodeSet
	}{
		{
			input: NodeSet{1, 2, 3},
			want:  0b1110,
		},
		{
			input: NodeSet{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16},
			want:  PackedNodeSet(0b11111111111111110),
		},
		{
			input: NodeSet{2, 4, 6, 8, 10, 12, 14, 16},
			want:  PackedNodeSet(0b10101010101010100),
		},
		{
			input: NodeSet{1, 3, 4, 7, 11, 15, 12, 18},
			want:  PackedNodeSet(0b1001001100010011010),
		},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("%v", tc.input),
			func(t *testing.T) {
				res := PackNodeSet(tc.input)

				if res != tc.want {
					t.Errorf(
						"Wrong output for %v\nwanted: %s\n   got: %s\n",
						tc.input, tc.want.ToBinary(), res.ToBinary())
					return
				}
			})
	}

}

func TestUnpackNodeSet(t *testing.T) {
	var cases = []struct {
		input PackedNodeSet
		want  NodeSet
	}{
		{
			input: PackedNodeSet(0b11111111111111110),
			want:  NodeSet{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16},
		},
		{
			input: PackedNodeSet(0b10101010101010100),
			want:  NodeSet{2, 4, 6, 8, 10, 12, 14, 16},
		},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("%v", tc),
			func(t *testing.T) {
				res := UnpackNodeSet(tc.input)

				if len(res) != len(tc.want) {
					t.Errorf(
						"Wrong output for %v\nwanted: %v\n   got: %v\n",
						tc.input, tc.want, res)
					return
				}

				for i := range tc.want {
					if tc.want[i] != res[i] {
						t.Errorf(
							"Wrong output for %v\nwanted: %v\n   got: %v\n",
							tc.input, tc.want, res)
						return
					}
				}
			})
	}
}

func TestPackUnpackNodeSet(t *testing.T) {
	var cases = []struct {
		input NodeSet
	}{
		{input: NodeSet{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("%v", tc.input),
			func(t *testing.T) {
				res := UnpackNodeSet(PackNodeSet(tc.input))

				for i, v := range res {
					if tc.input[i] != v {
						t.Errorf(
							"Wrong output for %v\nwanted: %v\n   got: %v\n",
							tc.input, tc.input, res)
						return
					}
				}
			})
	}
}

func TestPackUnpackNodeSetFuzz(t *testing.T) {
	t.Run("PackUnpackFuzz",
		func(t *testing.T) {
			for i := PackedNodeSet(1); i < 1<<16; i++ {
				res := PackNodeSet(UnpackNodeSet(i))
				if res != i {
					t.Errorf(
						"Wrong output for %v\nwanted: %v\n   got: %v\n",
						i, i, res)
					return
				}
			}
		})
}
