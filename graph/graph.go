package graph

import (
	"algobench/common"
	"fmt"
	"math"
	"math/rand"
	"time"
)

type cost float64
type angle float64
type radius float64

type node struct {
	x float64
	y float64
}

func Distance(n node, m node) float64 {
	return math.Sqrt(math.Pow(m.x-n.x, 2) + math.Pow(m.y-n.y, 2))
}

type Graph struct {
	nodes      []node
	costMatrix [][]cost
}

func (g *Graph) NodeCount() int {
	return len(g.costMatrix)
}

func (g *Graph) NodeSet() NodeSet {
	return NodeSet(series0toN(g.NodeCount()))
}

func (g *Graph) setCost(from int, to int, c cost) {
	g.costMatrix[from][to] = c
	g.costMatrix[to][from] = c
}

func (g *Graph) Cost(from int, to int) cost {
	return g.costMatrix[from][to]
}

// PathCost finds the total cost to traverse the graph
// through the specified path
func (g *Graph) PathCost(path []int) cost {
	pathCost := cost(0)
	for i := 1; i < len(path); i++ {
		pathCost += g.Cost(path[i-1], path[i])
	}

	return pathCost
}

func (g *Graph) LeastCostlyNodeFrom(start int, exclusions NodeSet) int {
	minCost := cost(math.MaxFloat64)
	minCostNode := math.MaxInt8

	for _, candidate := range g.NodeSet() {
		if common.Contains(exclusions, candidate) {
			continue
		}

		candidateCost := g.Cost(start, candidate)
		if candidateCost < minCost {
			minCost = candidateCost
			minCostNode = candidate
		}
	}
	return minCostNode
}

func (g *Graph) printCosts() {
	fmt.Printf("COSTS\n")
	for _, row := range g.costMatrix {
		for _, col := range row {
			fmt.Printf("% 8.3f ", col)
		}
		fmt.Printf("\n")
	}
	fmt.Printf("\n")
}

func (g *Graph) printNodes() {
	fmt.Printf("NODES\n")
	for i, node := range g.nodes {
		fmt.Printf("% 3d | (% 8.3f, % 8.3f)\n", i, node.x, node.y)
	}
	fmt.Printf("\n")
}

func emptyGraph(n uint64) Graph {
	g := make([][]cost, n)
	for row := range g {
		g[row] = make([]cost, n)
	}

	return Graph{costMatrix: g}
}

func MakeRandomGraph(n uint64, min float64, max float64) Graph {
	g := emptyGraph(n)

	g.nodes = make([]node, n)
	for i := range g.nodes {
		g.nodes[i] = node{0, 0}
	}

	for i := range g.nodes {
		for j := range g.nodes {
			g.setCost(i, j, cost(RandFloat64(min, max)))
		}
	}

	return g
}

func MakeRandomEuclidianGraph(n uint64, min float64, max float64) Graph {
	g := emptyGraph(n)

	g.nodes = make([]node, n)
	for i := range g.nodes {
		g.nodes[i] = node{
			math.Abs(RandFloat64(min, max)),
			math.Abs(RandFloat64(min, max))}
	}

	for i, n := range g.nodes {
		for j, m := range g.nodes {
			g.setCost(i, j, cost(Distance(n, m)))
		}
	}

	return g
}

func MakeRandomCirclularGraph(n uint64, r radius) Graph {
	g := emptyGraph(n)

	nodes := make([]node, n)

	for i := range nodes {
		theta := float64(i) * (math.Pi * 2) / float64(n)
		x := float64(r) * math.Cos(float64(theta))
		y := float64(r) * math.Sin(float64(theta))

		nodes[i] = node{x, y}
	}

	// Shuffle the nodes
	ra := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i, _ := range nodes {
		j := ra.Intn(i + 1)
		nodes[i], nodes[j] = nodes[j], nodes[i]
	}

	g.nodes = nodes

	for i, n := range g.nodes {
		for j, m := range g.nodes {
			g.setCost(i, j, cost(Distance(n, m)))
		}
	}

	return g
}
