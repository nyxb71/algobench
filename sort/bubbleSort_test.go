package sort

import (
	"algobench/common"
	"algobench/data"
	"sort"
	"testing"
)

func TestBubbleSortRandom(t *testing.T) {
	list := data.RandomList(common.TestListLength)
	sorted := BubbleSort(list)
	if !sort.IntsAreSorted(sorted) {
		t.Error(common.ErrorListNotSorted)
	}
}

func BenchmarkBubbleSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		list := data.RandomList(1000)
		b.StartTimer()
		BubbleSort(list)
		b.StopTimer()
	}
}
