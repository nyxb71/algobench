package str

import (
	"algobench/common"
	"fmt"
	"strings"
	"testing"
)

func TestLCS(t *testing.T) {
	implementations := []func(WordString, WordString) WordString{
		LCSBrute, LCSMatrix, LCSFragmentCache}

	for _, f := range implementations {
		var cases = []struct {
			a    string
			b    string
			want string
			mult bool // If there are multiple possible matches
		}{
			{a: "", b: "", want: "", mult: false},
			{a: "", b: "bar", want: "", mult: false},
			{a: "foo", b: "", want: "", mult: false},
			{a: "cat", b: "dog", want: "", mult: false},
			{a: "foocatbar", b: "catdog", want: "cat", mult: false},
			{a: "asdf", b: "asdf", want: "asdf", mult: false},
			{a: "ABABC", b: "BABCA", want: "babc", mult: false},
			{a: "BABCA", b: "ABCBA", want: "abc", mult: false},
			{a: "ABCBA", b: "ABABC", want: "abc", mult: false},
			{a: "foo bar", b: "bar", want: "bar", mult: false},
			{a: " ", b: "bar", want: "", mult: false},
			{
				// Long random with matching sub-string in middle
				a:    "rninchmduvecccyfpnbnkqyxcjgucsctesomukpmepdqsuslzvubesdqbcmy",
				b:    "qzbdjvnvugrkovartcsvrninchmduvecccyfpnbnlietiunqxkutkohjerlt",
				want: "rninchmduvecccyfpnbn",
				mult: false,
			},
			{
				// Multiple matches
				a:    " foo abz bar baz zab rab oof ",
				b:    " foo zba baz rab ",
				want: " baz ",
				mult: true,
			},
			{
				// len(want) > len(a) / 2
				a:    "The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog",
				b:    "foo over the lazy dog The quick brown fox bar",
				want: " over the lazy dog the quick brown fox ",
				mult: false,
			},
		}

		for _, tc := range cases {
			implName := common.GetFunctionName(f)
			t.Run(fmt.Sprintf("%s %v", implName, tc),
				func(t *testing.T) {
					aws, err := ToWordString(tc.a)
					if err != nil {
						t.Errorf("error converting: '%s'", tc.a)
					}

					bws, err := ToWordString(tc.b)
					if err != nil {
						t.Errorf("error converting: '%s'", tc.b)
					}

					lcs := f(aws, bws)
					wantLower := strings.ToLower(tc.want)
					// If the result is a different length, it is always wrong
					if lcs.Len() != len(wantLower) {
						t.Errorf(
							"Wrong output for '%s' and '%s'\nwanted: '%s'\n   got: '%s'\n",
							tc.a, tc.b, tc.want, lcs)
					}

					// If not expecting multiple possible matches, the result is wrong
					if lcs.String() != wantLower && !tc.mult {
						t.Errorf(
							"Wrong output for '%s' and '%s'\nwanted: '%s'\n   got: '%s'\n",
							tc.a, tc.b, tc.want, lcs)
					}
				})
		}
	}
}
