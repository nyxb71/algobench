# Install dependencies

    $ go get

# Run the program

    $ go run main.go [ARGUMENTS]
    
OR 

    $ go build && ./algobench [ARGUMENTS]

See --help for help

## Examples

    $ go run main.go --print --export sort merge quick
    $ go run main.go -pe sort merge quick
    $ go run main.go -pe sort merge quick -o resultsdir/
 
# Running tests

    $ go test -v ./...
    $ go test -v ./... -run=ThreeSum
