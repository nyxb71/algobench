package main

import (
	"algobench/bench"
	"os"

	"gopkg.in/alecthomas/kingpin.v2"
)

// Set up command line flag options
var (
	app = kingpin.New("algobench", "Benchmark some algorithms!")

	optExport = app.Flag(
		"export", "Export results to a file").Short('e').Bool()
	optOutput = app.Flag(
		"output", "Output path").Short('o').Default("results/").String()
	optPrint = app.Flag(
		"print", "Print results during computation").Short('p').Bool()
	optTrials = app.Flag("trials",
		"Number of trials for each input size").Short('t').Default("50").Uint64()

	searchComm = app.Command("search", "Benchmark search algorithms")
	searchArgs = searchComm.Arg(
		"algorithms", "Names of the algorithms to benchmark").Enums(
		bench.SearchTimers.Keys()...)

	sortComm = app.Command("sort", "Benchmark sort algorithms")
	sortArgs = sortComm.Arg(
		"algorithms", "Names of the algorithms to benchmark").Enums(
		bench.SortTimers.Keys()...)

	miscComm = app.Command("misc", "Benchmark misc algorithms")
	miscArgs = miscComm.Arg(
		"algorithms", "Names of the algorithms to benchmark").Enums(
		bench.MiscTimers.Keys()...)

	bigIntComm = app.Command("bigint", "Benchmark BigInt algorithms")
	bigIntArgs = bigIntComm.Arg(
		"algorithms", "Names of the algorithms to benchmark").Enums(
		bench.BigIntTimers.Keys()...)

	lcsComm = app.Command("lcs", "Benchmark LCS algorithms")
	lcsArgs = lcsComm.Arg(
		"algorithms", "Names of the algorithms to benchmark").Enums(
		bench.LCSTimers.Keys()...)

	tspComm = app.Command("tsp", "Benchmark TSP algorithms")
	tspArgs = tspComm.Arg(
		"algorithms", "Names of the algorithms to benchmark").Enums(
		bench.TSPTimers.Keys()...)
)

func main() {
	// Turn the garbage collector off
	// ONLY FOR EXPERIMENTAL RUNS
	// debug.SetGCPercent(-1)

	kingpin.Version("0.0.2")
	res := kingpin.MustParse(app.Parse(os.Args[1:]))
	options := bench.Options{
		Export:    *optExport,
		Output:    *optOutput,
		Print:     *optPrint,
		NumTrials: *optTrials}
	switch res {
	case searchComm.FullCommand():
		bench.HandleRequests(*searchArgs, "search", options)
	case sortComm.FullCommand():
		bench.HandleRequests(*sortArgs, "sort", options)
	case miscComm.FullCommand():
		bench.HandleRequests(*miscArgs, "misc", options)
	case bigIntComm.FullCommand():
		bench.HandleRequests(*bigIntArgs, "bigint", options)
	case lcsComm.FullCommand():
		bench.HandleRequests(*lcsArgs, "lcs", options)
	case tspComm.FullCommand():
		bench.HandleRequests(*tspArgs, "tsp", options)
	}

	os.Exit(0)
}
