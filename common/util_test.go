package common

import (
	"fmt"
	"math"
	"testing"
)

func TestSplitExponent(t *testing.T) {
	var cases = []struct {
		input uint64
		wanta uint64
		wantb uint64
	}{
		{input: 0, wanta: 0, wantb: 0},
		{input: 1, wanta: 1, wantb: 0},
		{input: 2, wanta: 1, wantb: 1},
		{input: 3, wanta: 2, wantb: 1},
		{input: 7, wanta: 4, wantb: 3},
		{input: 8, wanta: 4, wantb: 4},
		{input: 11, wanta: 8, wantb: 3},
		{input: 13, wanta: 8, wantb: 5},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("%v", tc.input),
			func(t *testing.T) {
				a, b := SplitExponent(tc.input)

				if a != tc.wanta || b != tc.wantb {
					t.Errorf("Wrong output, input: %v, wanted: (%d,%d), got: (%d,%d)",
						tc.input, tc.wanta, tc.wantb, a, b)
				}
			})
	}
}

func TestBitsToRepresent(t *testing.T) {
	var cases = []struct {
		input uint64
		want  uint64
	}{
		{input: 0, want: 1},
		{input: 1, want: 1},
		{input: 2, want: 2},
		{input: 3, want: 2},
		{input: 4, want: 3},
		{input: 8, want: 4},
		{input: math.MaxUint64, want: 64},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("%v", tc.input),
			func(t *testing.T) {
				bits := BitsToRepresent(tc.input)

				if bits != tc.want {
					t.Errorf("Wrong output, input: %d, wanted: %d, got: %d",
						tc.input, tc.want, bits)
				}
			})
	}
}
