package sort

// BubbleSort sorts a list in place
func BubbleSort(list []int) []int {
	size := len(list)
	swapped := true
	for swapped {
		swapped = false
		for i := 1; i < size; i++ {
			if list[i-1] > list[i] {
				list[i-1], list[i] = list[i], list[i-1]
				swapped = true
			}
		}
	}

	return list
}
