package sort

import (
	"math/rand"
	"sort"
)

func permute(src []int) []int {
	// https://stackoverflow.com/a/12264918

	dest := make([]int, len(src))
	destIndexes := rand.Perm(len(src))
	for index, val := range destIndexes {
		dest[val] = src[index]
	}
	return dest
}

func BogoSort(list []int) []int {
	for !sort.IntsAreSorted(list) {
		list = permute(list)
	}
	return list
}
