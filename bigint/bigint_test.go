package bigint

import (
	"fmt"
	"math"
	"reflect"
	"testing"
)

// Test a + b for a, b = [-128,127]
func TestAddFuzz(t *testing.T) {
	t.Run("FuzzAdd",
		func(t *testing.T) {
			for i := int64(0); i < math.MaxInt8; i++ {
				for j := int64(0); j < math.MaxInt8; j++ {
					want := FromInt(i+j, 10)
					digits := add(FromInt(i, 10), FromInt(j, 10), 10)

					if !reflect.DeepEqual(digits, want) {
						t.Errorf(
							"Wrong output for %d + %d\nwanted: %v\n   got: %v\n",
							i, j, want, digits)
						return
					}
				}
			}
		})
}

func TestAddTableString(t *testing.T) {
	var cases = []struct {
		a    string
		b    string
		want string
	}{
		{
			a:    "999",
			b:    "999",
			want: "1998",
		},
		{
			a:    "11111111111111111111111111111111",
			b:    "22222222222222222222222222222222",
			want: "33333333333333333333333333333333",
		},
		{
			a:    "10000000000000000000000000000000",
			b:    "20000000000000000000000000000000",
			want: "30000000000000000000000000000000",
		},
		{
			a:    "99999999999999999999999999999999",
			b:    "99999999999999999999999999999999",
			want: "199999999999999999999999999999998",
		},
		{
			a:    "-99999999999999999999999999999999",
			b:    "99999999999999999999999999999999",
			want: "0",
		},
		{
			a:    "-99999999999999999999999999999999",
			b:    "-99999999999999999999999999999999",
			want: "-199999999999999999999999999999998",
		},
		{
			a:    "10101010101010101010101010101010",
			b:    "01010101010101010101010101010101",
			want: "11111111111111111111111111111111",
		},
		{
			a:    "10101010101010101010101010101010",
			b:    "10101010101010101010101010101010",
			want: "20202020202020202020202020202020",
		},
		{
			a:    "11111111111111111111111111111111",
			b:    "33333333333333333333333333333333",
			want: "44444444444444444444444444444444",
		},
		{
			a:    "99999999999999999999999999999999",
			b:    "1",
			want: "100000000000000000000000000000000",
		},
		{
			a:    "-99999999999999999999999999999999",
			b:    "1",
			want: "-99999999999999999999999999999998",
		},
		{
			a:    "23232323232323232323232323232323",
			b:    "45454545454545454545454545454545",
			want: "68686868686868686868686868686868",
		},
		{
			a:    "87878787878787878787878787878787",
			b:    "21212121212121212121212121212121",
			want: "109090909090909090909090909090908",
		},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("\n%v\n*\n%v\n=\n%v", tc.a, tc.b, tc.want),
			func(t *testing.T) {
				res := add(
					IntStr(tc.a).Digits(10),
					IntStr(tc.b).Digits(10),
					10).String(10)

				if res != tc.want {
					t.Errorf(
						"Wrong output for\n%v\n*\n%v\nwanted: %v\n   got: %v\n",
						tc.a, tc.b, tc.want, res)
				}
			})
	}
}

func TestMultTableDigits(t *testing.T) {
	var cases = []struct {
		a    Digits
		b    Digits
		want Digits
	}{
		{a: Digits{1}, b: Digits{0}, want: Digits{0}},
		{a: Digits{1}, b: Digits{1}, want: Digits{1}},
		{a: Digits{2}, b: Digits{1}, want: Digits{2}},
		{a: Digits{2}, b: Digits{2}, want: Digits{4}},
		{a: Digits{9}, b: Digits{9}, want: Digits{8, 1}},
		{a: Digits{2}, b: Digits{9}, want: Digits{1, 8}},
		{a: Digits{9, 9, 9}, b: Digits{9, 9, 9}, want: Digits{9, 9, 8, 0, 0, 1}},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("%v * %v = %v", tc.a, tc.b, tc.want),
			func(t *testing.T) {
				res := mult(tc.a, tc.b, 10)

				if !reflect.DeepEqual(res, tc.want) {
					t.Errorf(
						"Wrong output for (%v*%v), wanted: %v, got: %v\n",
						tc.a, tc.b, tc.want, res)
				}
			})
	}
}

func TestMultTableString(t *testing.T) {
	var cases = []struct {
		a    string
		b    string
		want string
	}{
		{
			a:    "999",
			b:    "999",
			want: "998001",
		},
		{
			a:    "11111111111111111111111111111111",
			b:    "22222222222222222222222222222222",
			want: "246913580246913580246913580246908641975308641975308641975308642",
		},
		{
			a:    "10000000000000000000000000000000",
			b:    "20000000000000000000000000000000",
			want: "200000000000000000000000000000000000000000000000000000000000000",
		},
		{
			a:    "99999999999999999999999999999999",
			b:    "99999999999999999999999999999999",
			want: "9999999999999999999999999999999800000000000000000000000000000001",
		},
		{
			a:    "-99999999999999999999999999999999",
			b:    "-99999999999999999999999999999999",
			want: "9999999999999999999999999999999800000000000000000000000000000001",
		},
		{
			a:    "10101010101010101010101010101010",
			b:    "01010101010101010101010101010101",
			want: "10203040506070809101112131415161514131211100908070605040302010",
		},
		{
			a:    "10101010101010101010101010101010",
			b:    "2",
			want: "20202020202020202020202020202020",
		},
		{
			a:    "10101010101010101010101010101010",
			b:    "-2",
			want: "-20202020202020202020202020202020",
		},
		{
			a:    "99999999999999999999999999999999",
			b:    "1",
			want: "99999999999999999999999999999999",
		},
		{
			a:    "99999999999999999999999999999999",
			b:    "9",
			want: "899999999999999999999999999999991",
		},
		{
			a:    "23232323232323232323232323232323",
			b:    "45454545454545454545454545454545",
			want: "1056014692378328741965105601469216712580348943985307621671258035",
		},
		{
			a:    "87878787878787878787878787878787",
			b:    "21212121212121212121212121212121",
			want: "1864095500459136822773186409550008631772268135904499540863177227",
		},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("\n%v\n*\n%v\n=\n%v", tc.a, tc.b, tc.want),
			func(t *testing.T) {
				res := mult(
					IntStr(tc.a).Digits(10),
					IntStr(tc.b).Digits(10),
					10).String(10)

				if res != tc.want {
					t.Errorf(
						"Wrong output for\n%v\n*\n%v\nwanted: %v\n   got: %v\n",
						tc.a, tc.b, tc.want, res)
				}
			})
	}
}

// Test a * b for a, b = [-128,127]
func TestMultFuzz(t *testing.T) {
	t.Run("MultFuzz",
		func(t *testing.T) {
			for i := int64(0); i < math.MaxInt8; i++ {
				for j := int64(0); j < math.MaxInt8; j++ {
					want := FromInt(i*j, 10)
					digits := mult(FromInt(i, 10), FromInt(j, 10), 10)

					if !reflect.DeepEqual(digits, want) {
						t.Errorf(
							"Wrong output for %d * %d\nwanted: %v\n   got: %v\n",
							i, j, want, digits)
						return
					}
				}
			}
		})
}

func TestSubTableString(t *testing.T) {
	var cases = []struct {
		a    string
		b    string
		want string
	}{
		{
			a:    "999",
			b:    "999",
			want: "0",
		},
		{
			a:    "11111111111111111111111111111111",
			b:    "22222222222222222222222222222222",
			want: "-11111111111111111111111111111111",
		},
		{
			a:    "10000000000000000000000000000000",
			b:    "20000000000000000000000000000000",
			want: "-10000000000000000000000000000000",
		},
		{
			a:    "99999999999999999999999999999999",
			b:    "99999999999999999999999999999999",
			want: "0",
		},
		{
			a:    "-99999999999999999999999999999999",
			b:    "99999999999999999999999999999999",
			want: "-199999999999999999999999999999998",
		},
		{
			a:    "-99999999999999999999999999999999",
			b:    "-99999999999999999999999999999999",
			want: "0",
		},
		{
			a:    "10101010101010101010101010101010",
			b:    "01010101010101010101010101010101",
			want: "9090909090909090909090909090909",
		},
		{
			a:    "10101010101010101010101010101010",
			b:    "10101010101010101010101010101010",
			want: "0",
		},
		{
			a:    "11111111111111111111111111111111",
			b:    "33333333333333333333333333333333",
			want: "-22222222222222222222222222222222",
		},
		{
			a:    "99999999999999999999999999999999",
			b:    "1",
			want: "99999999999999999999999999999998",
		},
		{
			a:    "-99999999999999999999999999999999",
			b:    "1",
			want: "-100000000000000000000000000000000",
		},
		{
			a:    "23232323232323232323232323232323",
			b:    "45454545454545454545454545454545",
			want: "-22222222222222222222222222222222",
		},
		{
			a:    "87878787878787878787878787878787",
			b:    "21212121212121212121212121212121",
			want: "66666666666666666666666666666666",
		},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("\n%v\n*\n%v\n=\n%v", tc.a, tc.b, tc.want),
			func(t *testing.T) {
				res := sub(
					IntStr(tc.a).Digits(10),
					IntStr(tc.b).Digits(10),
					10).String(10)

				if res != tc.want {
					t.Errorf(
						"Wrong output for\n%v\n-\n%v\nwanted: %v\n   got: %v\n",
						tc.a, tc.b, tc.want, res)
				}
			})
	}
}

// Test a - b for a, b = [-128,127]
func TestSubFuzz(t *testing.T) {
	t.Run("SubFuzz",
		func(t *testing.T) {
			for i := int64(math.MinInt8); i <= math.MaxInt8; i++ {
				for j := int64(math.MinInt8); j <= math.MaxInt8; j++ {
					want := FromInt(i-j, 10)
					digits := sub(FromInt(i, 10), FromInt(j, 10), 10)

					if !reflect.DeepEqual(digits, want) {
						t.Errorf(
							"Wrong output for (%d-%d)\nwanted: %v\n   got: %v\n",
							i, j, want, digits)
						return
					}
				}
			}
		})
}
