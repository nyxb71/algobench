package threesum

import (
	"algobench/common"
	"fmt"
	"testing"
)

func TestThreeSumCount(t *testing.T) {
	// For each implementation of ThreeSum, run each test case
	implementations := []func([]int) uint{ThreeSum, ThreeSumFast,
		ThreeSumFasterA, ThreeSumFasterB}
	for _, f := range implementations {
		var threeSumCases = []struct {
			list     []int
			expected uint
		}{
			// Input array, expected value
			{[]int{0, -1, 2, -3, 1}, 2},
			{[]int{0, 1, -1, 2}, 1},
			{[]int{2, -2, 3, -3, 0}, 2},
			{[]int{2, -2, 3, -3, 3}, 0},
			{[]int{1, -1, 0, 2, -2, 3, -3}, 5},
			{[]int{0, 3, -3, 5, -5, 7, -7, 11, -11}, 4},
			{[]int{0, 1, 2}, 0},
			{[]int{1, 2, 3}, 0},
		}

		// Test each case
		for _, elem := range threeSumCases {
			implName := common.GetFunctionName(f)
			t.Run(fmt.Sprintf(
				"Case: %s %v = %d", implName, elem.list, elem.expected),
				func(t *testing.T) {
					count := f(elem.list)
					if count != elem.expected {
						t.Errorf(
							"Wrong count for %s(%v), expected: %d, got: %d\n",
							implName, elem.list, elem.expected, count)
					}
				})
		}
	}
}
