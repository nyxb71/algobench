package data

import (
	"math"
	"math/rand"
	"time"

	"github.com/MichaelTJones/pcg"
)

// SeriesExponential generates an exponential series base^start..base^end
func SeriesExponential(base uint64, start uint64, limit uint64) []uint64 {
	var series []uint64
	for i := start; i <= limit; i++ {
		series = append(series, uint64(math.Pow(float64(base), float64(i))))
	}
	return series
}

// AscendingList generates an ascending list
func AscendingList(length uint64) []int {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	list := make([]int, length)
	list[0] = r.Intn(10)
	for i := 1; i < int(length); i++ {
		offset := r.Intn(9)
		// Go up by 1..10 each step
		list[i] = list[i-1] + 1 + offset
	}

	return list
}

// RandomList generates an unordered list of length n,
// with values ranging from 1..length
func RandomList(length uint64) []int {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	return r.Perm(int(length))
}

// RandomListWithNegatives generates an unordered list with negatives
func RandomListWithNegatives(length uint64) []int {
	list := make([]int, length)
	p := pcg.NewPCG32()
	for i := uint64(0); i < length; i++ {
		b := int(p.Random())
		flip := int8(p.Random() & 0x01)
		// If flip (a random boolean) is false, make the number negative
		if flip == 0 {
			list[i] = b
		} else {
			list[i] = 0 - b
		}
	}

	return list
}

// GetRandomElementAndIndex finds a random element in the list,
// and returns the element with its index
func GetRandomElementAndIndex(list []int) (int, uint64) {
	maxIndex := len(list) - 1
	elemIndex := uint64(rand.Intn(maxIndex))
	return list[elemIndex], elemIndex
}

func SeriesMToN(m uint64, n uint64) []uint64 {
	series := []uint64{}

	if m == n {
		return append(series, m)
	}

	if n < m {
		for i := m; i >= n; i-- {
			series = append(series, i)
		}
		return series
	}

	for i := m; i <= n; i++ {
		series = append(series, i)
	}

	return series
}
