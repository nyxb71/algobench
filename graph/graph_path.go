package graph

import (
	"fmt"
	"math"
)

type GraphPath struct {
	path NodeSet
	c    cost
}

func appendNodeToGraphPath(g *Graph, gp *GraphPath, n int) {
	hopCost := g.Cost(gp.path[len(gp.path)-1], n)
	gp.path = append(gp.path, n)
	gp.c += hopCost
}

func getGraphPath(g *Graph, ns NodeSet) GraphPath {
	return GraphPath{ns, g.PathCost(ns)}
}

func initGraphPath(nodeCount int) GraphPath {
	return GraphPath{make([]int, nodeCount), cost(math.MaxInt32)}
}

func (gp *GraphPath) Print(g *Graph) {
	totalDist := cost(0)

	for i := 1; i < len(gp.path); i++ {
		a := g.nodes[gp.path[i-1]]
		b := g.nodes[gp.path[i]]
		dist := Distance(a, b)
		totalDist += cost(dist)

		fmt.Printf("% 3d -> % 3d | (% 8.3f, % 8.3f) -> (% 8.3f, % 8.3f) | Cost: %.3f \n",
			gp.path[i-1], gp.path[i], a.x, a.y, b.x, b.y, dist)
	}
	fmt.Printf("Total Cost: %.3f\n", totalDist)

}
