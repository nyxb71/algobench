package threesum

import (
	"algobench/search"
	"sort"
)

// ThreeSum is the brute force version of ThreeSum
func ThreeSum(list []int) uint {
	n := len(list)

	count := uint(0)
	for i := 0; i < n; i++ {
		for j := i + 1; j < n; j++ {
			for k := j + 1; k < n; k++ {
				if list[i]+list[j]+list[k] == 0 {
					count++
				}
			}
		}
	}
	return count
}

// ThreeSumFast is a slightly smarter version of ThreeSum
func ThreeSumFast(list []int) uint {
	n := len(list)
	// Prepare the list by sorting it
	sort.Ints(list)

	count := uint(0)
	for i := 0; i < n; i++ {
		for j := i + 1; j < n; j++ {
			// Calculate the target
			// (negative of the sum of the current two numbers)
			target := -(list[i] + list[j])
			// Search for the target in the list
			index, err := search.BinarySearch(list, target)
			// If it was found, and it is further in the list
			// than the current value of j, increase the count
			if err == nil && index > j {
				count++
			}
		}
	}
	return count
}

// ThreeSumFasterA is another optimization of ThreeSum that uses
// a value->bool hashmap
func ThreeSumFasterA(list []int) uint {
	n := len(list)

	count := uint(0)
	for i := 0; i < n; i++ {
		// For each i, keep track of which values of j we have seen
		seen := make(map[int]bool, n)
		for j := i + 1; j < n; j++ {
			// What we need to sum to 0
			target := -(list[i] + list[j])
			// If we have seen that number before, increase the count
			if seen[target] {
				count++
			} else {
				// If we have not seen that number before, add it to the cache
				seen[list[j]] = true
			}
		}
	}

	return count
}

// ThreeSumFasterB is another optimization of ThreeSum that uses
// a value->index hashmap
func ThreeSumFasterB(list []int) uint {
	n := len(list)

	// Create a map of values -> index for quick lookups (constant time)
	indexOf := make(map[int]int, n)
	for i := 0; i < n; i++ {
		indexOf[list[i]] = i
	}

	// For each pair i, j, if the negative of their sum is in the list
	// and it's index is after j, we have found a match
	count := uint(0)
	for i := 0; i < n; i++ {
		for j := i + 1; j < n; j++ {
			target := -(list[i] + list[j])
			if indexOf[target] > j {
				count++
			}
		}
	}

	return count
}
