package bench

import (
	"time"
)

// Trial represents benchmark metadata for a function
// at a single input size
type Trial struct {
	F               TimerFunc
	NumTrials       uint64
	InputSize       uint64
	TotalTime       time.Duration
	AverageTime     time.Duration
	RatioToPrevious float64
}

// MakeTrial constructs a Trial
func MakeTrial(f TimerFunc, numTrials uint64, inputSize uint64) Trial {
	return Trial{f, numTrials, inputSize, time.Duration(0), time.Duration(0), 1}
}

// Run a trial
func (t *Trial) Run() {
	t.TotalTime = time.Duration(0)
	for i := uint64(0); i < t.NumTrials; i++ {
		t.TotalTime += t.F(t.InputSize)
	}

	t.AverageTime = t.TotalTime / (time.Duration(t.NumTrials) * time.Nanosecond)
}
