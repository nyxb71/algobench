package matrix

import (
	"testing"
)

var I2, _ = Identity(2)
var I3, _ = Identity(3)
var A2, _ = From2DArray([][]uint64{{1, 1}, {1, 1}})
var B2, _ = From2DArray([][]uint64{{1, 2}, {3, 4}})

var A2PlusB2, _ = From2DArray([][]uint64{{2, 3}, {4, 5}})
var A2TimesB2, _ = From2DArray([][]uint64{{4, 6}, {4, 6}})
var B2TimesA2, _ = From2DArray([][]uint64{{3, 3}, {7, 7}})

var I2PlusI2, _ = From2DArray([][]uint64{{2, 0}, {0, 2}})
var I3PlusI3, _ = From2DArray([][]uint64{{2, 0, 0}, {0, 2, 0}, {0, 0, 2}})

var RA, _ = From2DArray([][]uint64{{1, 2, 3}, {4, 5, 6}})
var RB, _ = From2DArray([][]uint64{{7, 8}, {9, 10}, {11, 12}})
var RATimesRB, _ = From2DArray([][]uint64{{58, 64}, {139, 154}})

func TestMatrixEqual(t *testing.T) {
	var cases = []struct {
		name string
		a    Matrix
		b    Matrix
		want bool
	}{
		{name: "I2 = I2", a: I2, b: I2, want: true},
		{name: "I3 = I3", a: I3, b: I3, want: true},
		{name: "I2 = I3", a: I2, b: I3, want: false},
		{name: "A2 = B2", a: A2, b: B2, want: false},
	}

	for _, tc := range cases {
		t.Run(tc.name,
			func(t *testing.T) {
				eq, err := Equal(tc.a, tc.b)
				if eq != tc.want {
					t.Errorf("Wrong output for %s\nwanted: %t, got: %t\nerror: %s\n",
						tc.name, tc.want, eq, err)
				}
			})
	}
}

func TestMatrixAdd(t *testing.T) {
	var cases = []struct {
		name string
		a    Matrix
		b    Matrix
		want Matrix
	}{
		{name: "I2 + I2", a: I2, b: I2, want: I2PlusI2},
		{name: "I3 + I3", a: I3, b: I3, want: I3PlusI3},
		{name: "A2 + B2", a: A2, b: B2, want: A2PlusB2},
	}

	for _, tc := range cases {
		t.Run(tc.name,
			func(t *testing.T) {
				output, err := Add(tc.a, tc.b)
				if err != nil {
					t.Error(err)
				}
				eq, err := Equal(output, tc.want)
				if !eq {
					t.Errorf(
						"Wrong output for (%v * %v)\nwanted: %v, got: %v\nerror: %s\n",
						tc.a.cells, tc.b.cells, tc.want.cells, output.cells, err)
				}
			})
	}
}

func TestDotProduct(t *testing.T) {
	var multCases = []struct {
		name string
		a    []uint64
		b    []uint64
		want uint64
	}{
		{name: "{1,1} . {1,1} = 2", a: []uint64{1, 1}, b: []uint64{1, 1}, want: 2},
		{name: "{1,2} . {3,4} = 11", a: []uint64{1, 2}, b: []uint64{3, 4}, want: 11},
	}

	for _, tc := range multCases {
		t.Run(tc.name,
			func(t *testing.T) {
				output, err := dotProduct(tc.a, tc.b)
				if err != nil {
					t.Error(err)
				}
				if output != tc.want {
					t.Errorf(
						"Wrong output for (%v . %v)\nwanted: %d\n   got: %d\n",
						tc.a, tc.b, tc.want, output)
				}
			})
	}
}

func TestMatrixMult(t *testing.T) {
	var multCases = []struct {
		name string
		a    Matrix
		b    Matrix
		want Matrix
	}{
		// Square matrices
		{name: "I2 * I2", a: I2, b: I2, want: I2},
		{name: "I3 * I3", a: I3, b: I3, want: I3},
		{name: "A2 * B2", a: A2, b: B2, want: A2TimesB2},
		{name: "B2 * A2", a: B2, b: A2, want: B2TimesA2},
		// Rectangular matrices
		{name: "RA * RB", a: RA, b: RB, want: RATimesRB},
	}

	for _, tc := range multCases {
		t.Run(tc.name,
			func(t *testing.T) {
				output, err := Mult(tc.a, tc.b)
				if err != nil {
					t.Error(err)
				}
				eq, err := Equal(output, tc.want)
				if !eq {
					t.Errorf(
						"Wrong output for (%v * %v)\nwanted: %v\n   got: %v\nerror: %s\n",
						tc.a.cells, tc.b.cells, tc.want.cells, output.cells, err)
				}
			})
	}
}
