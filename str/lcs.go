package str

import (
	"strings"
)

func min(a int, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a int, b int) int {
	if a >= b {
		return a
	}
	return b
}

func longerShorter(a WordString, b WordString) (WordString, WordString) {
	if len(a) >= len(b) {
		return a, b
	}
	return b, a
}

func LCSBrute(a WordString, b WordString) WordString {
	longer, shorter := longerShorter(a, b)
	lenLonger, lenShorter := len(longer), len(shorter)
	if lenShorter == 0 || lenLonger == 0 {
		return WordString("")
	}

	// Starting from the complete version of the shorter string,
	// create a "window" starting at "left", through "right".
	//
	// Gradually reduce the window length and position, checking if the window
	// contexts exist in the other string. Keep track of the longest match.
	//
	// Example: a = 'foocatbar' b = 'catdog'
	// sub:
	// catdog, catdo, catd, cat, ca, c,
	// atdog,  atdo,  atd,  at,  a,
	// tdog,   tdo,   td,   t,
	// dog,    do,    d,
	// og,     g
	// g

	LCS := WordString("")
	for left := 0; left < lenShorter; left++ {
		for right := lenShorter; right > left; right-- {
			sub := shorter[left:right]
			if strings.Contains(string(longer), string(sub)) &&
				len(sub) > len(LCS) {
				LCS = sub
			}
		}
	}

	return LCS
}

func lcsFragmentCachePopulate(cache fragmentCache, text WordString, target WordString) {
	// Base case: Since we already have single characters cached,
	// we can just return here
	if len(target) <= 1 {
		return
	}

	// Get the indicies for the first character of target
	first := string(target[0])
	indicies, ok := cache[first]
	// If there weren't any, advance the target
	if !ok {
		lcsFragmentCachePopulate(cache, text, target[1:])
		return
	}

	for _, fragmentStart := range indicies {
		for i := 1; i < len(target); i++ {
			fragmentEnd := fragmentStart + i
			if fragmentEnd >= len(text) {
				continue
			}

			// If the character in the text doesn't match the target,
			// we can stop looking here
			if text[fragmentEnd] != target[i] {
				break
			}

			cache.insert(
				string(text[fragmentStart:fragmentEnd+1]),
				fragmentStart)
		}
	}

	// Narrow the target window
	lcsFragmentCachePopulate(cache, text, target[1:])
}

func LCSFragmentCache(a WordString, b WordString) WordString {
	if len(a) == 0 || len(b) == 0 {
		return WordString("")
	}

	// Concept 2: Start from the smallest elements and work up.
	// Use a recursive strategy and a cache.

	text, target := longerShorter(a, b)

	cache := fragmentCache{}

	// Pre-populate the cache with letters present in both strings
	for i, r := range text {
		if target.Contains(WordString(r)) {
			cache.insert(string(r), i)
		}
	}

	// If there were no common letters, the LCS is the empty string
	if len(cache) == 0 {
		return WordString("")
	}

	// Fully populate the cache
	lcsFragmentCachePopulate(cache, text, target)

	return WordString(cache.longestKey())
}

func LCSMatrix(a WordString, b WordString) WordString {
	// https://www.youtube.com/watch?v=aVFWW3pBQFo

	if len(a) == 0 || len(b) == 0 {
		return WordString("")
	}

	// Initialize the cache
	cache := make([][]int, len(a))
	for i := 0; i < len(a); i++ {
		cache[i] = make([]int, len(b))
	}

	LCS := WordString("")
	lenLCS := 0
	for row := 0; row < len(a); row++ {
		for col := 0; col < len(b); col++ {
			if a[row] == b[col] {
				// If at the start of a or b
				// IE: a[0] or b[0]
				if row == 0 || col == 0 {
					cache[row][col] = 1
				} else {
					// The new LCS length is the 1 + the previous length
					// (the value to the top-left in the matrix)
					cache[row][col] = cache[row-1][col-1] + 1
				}

				curLenLCS := cache[row][col]
				// If a longer LCS has been found, store it
				if curLenLCS > lenLCS {
					lenLCS = curLenLCS
					startLCS := row - lenLCS + 1
					endLCS := startLCS + lenLCS
					LCS = WordString(a[startLCS:endLCS])
				}
			}
		}
	}

	return LCS
}
