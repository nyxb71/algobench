package bigmatrix

import "testing"

var I2, _ = Identity(2)
var I3, _ = Identity(3)

var A2, _ = From2DArray([][]uint64{{1, 1}, {1, 1}})
var B2, _ = From2DArray([][]uint64{{1, 2}, {3, 4}})

var A2TimesB2, _ = From2DArray([][]uint64{{4, 6}, {4, 6}})
var B2TimesA2, _ = From2DArray([][]uint64{{3, 3}, {7, 7}})

func TestBigMatrixEqual(t *testing.T) {
	var cases = []struct {
		name string
		a    Matrix
		b    Matrix
		want bool
	}{
		{name: "I2 = I2", a: I2, b: I2, want: true},
		{name: "I3 = I3", a: I3, b: I3, want: true},
		{name: "I2 = I3", a: I2, b: I3, want: false},
		{name: "A2 = B2", a: A2, b: B2, want: false},
	}

	for _, tc := range cases {
		t.Run(tc.name,
			func(t *testing.T) {
				eq, err := Equal(tc.a, tc.b)
				if eq != tc.want {
					t.Errorf("Wrong output for %s\nwanted: %t, got: %t\nerror: %s\n",
						tc.name, tc.want, eq, err)
				}
			})
	}
}

func TestBigMatrixMult(t *testing.T) {
	var multCases = []struct {
		name string
		a    Matrix
		b    Matrix
		want Matrix
	}{
		// Square matrices
		{name: "I2 * I2", a: I2, b: I2, want: I2},
		{name: "A2 * B2", a: A2, b: B2, want: A2TimesB2},
		{name: "B2 * A2", a: B2, b: A2, want: B2TimesA2},
	}

	for _, tc := range multCases {
		t.Run(tc.name,
			func(t *testing.T) {
				output, err := Mult2x2(tc.a, tc.b)
				if err != nil {
					t.Error(err)
				}
				eq, err := Equal(output, tc.want)
				if !eq {
					t.Errorf(
						"Wrong output for (%v * %v)\nwanted: %v\n   got: %v\nerror: %s\n",
						tc.a.cells, tc.b.cells, tc.want.cells, output.cells, err)
				}
			})
	}
}
