package bigint

import (
	"fmt"
	"reflect"
	"testing"
)

func TestReversed(t *testing.T) {
	var cases = []struct {
		a    Digits
		want Digits
	}{
		{a: Digits{0}, want: Digits{0}},
		{a: Digits{1}, want: Digits{1}},
		{a: Digits{1, 2, 3}, want: Digits{3, 2, 1}},
		{a: Digits{1, 1, 1}, want: Digits{1, 1, 1}},
		{a: Digits{3, 2, 1}, want: Digits{1, 2, 3}},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("%v", tc.a),
			func(t *testing.T) {
				digits := tc.a.reversed()
				if !reflect.DeepEqual(digits, tc.want) {
					t.Errorf(
						"Wrong output for %v, wanted: %v, got: %v\n",
						tc.a, tc.want, digits)
				}
			})
	}
}

func TestNormalizeSigns(t *testing.T) {
	var cases = []struct {
		a    Digits
		want Digits
	}{
		{a: Digits{0}, want: Digits{0}},
		{a: Digits{1}, want: Digits{1}},
		{a: Digits{-1}, want: Digits{-1}},
		{a: Digits{-9}, want: Digits{-9}},
		{a: Digits{-1, 1}, want: Digits{0, -9}},
		{a: Digits{-1, -2}, want: Digits{-1, -2}},
		{a: Digits{-1, 0, 0}, want: Digits{-1, 0, 0}},
		{a: Digits{-1, -2, -3}, want: Digits{-1, -2, -3}},
		{a: Digits{1, 2, 3}, want: Digits{1, 2, 3}},
		{a: Digits{1, 0, -1}, want: Digits{0, 9, 9}},
		{a: Digits{1, -1, 9}, want: Digits{0, 9, 9}},
		{a: Digits{0, 9, 9}, want: Digits{0, 9, 9}},
		{a: Digits{0, 1, -1, 9}, want: Digits{0, 0, 9, 9}},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("%v", tc.a),
			func(t *testing.T) {
				digits := tc.a.normalizeSigns(10)
				if !reflect.DeepEqual(digits, tc.want) {
					t.Errorf(
						"Wrong output for %v\nwanted: %v\n   got: %v\n",
						tc.a, tc.want, digits)
				}
			})
	}
}

func TestCarryOverflows(t *testing.T) {
	var cases = []struct {
		a    Digits
		want Digits
	}{
		{a: Digits{0}, want: Digits{0}},
		{a: Digits{1}, want: Digits{1}},
		{a: Digits{-1}, want: Digits{-1}},
		{a: Digits{-9}, want: Digits{-9}},
		{a: Digits{-1, -2}, want: Digits{-1, -2}},
		{a: Digits{-1, 0, 0}, want: Digits{-1, 0, 0}},
		{a: Digits{-1, -2, -3}, want: Digits{-1, -2, -3}},
		{a: Digits{1, 2, 3}, want: Digits{1, 2, 3}},
		{a: Digits{1, 0}, want: Digits{1, 0}},
		{a: Digits{0, 1}, want: Digits{0, 1}},

		{a: Digits{-10}, want: Digits{-1, 0}},
		{a: Digits{-11}, want: Digits{-1, -1}},
		{a: Digits{-18}, want: Digits{-1, -8}},
		{a: Digits{-19}, want: Digits{-1, -9}},
		{a: Digits{-20}, want: Digits{-2, 0}},
		{a: Digits{-21}, want: Digits{-2, -1}},
		{a: Digits{-18, 0}, want: Digits{-1, -8, 0}},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("%v", tc.a),
			func(t *testing.T) {
				digits := tc.a.carryOverflows(10)
				if !reflect.DeepEqual(digits, tc.want) {
					t.Errorf(
						"Wrong output for %v\nwanted: %v\n   got: %v\n",
						tc.a, tc.want, digits)
				}
			})
	}
}

func TestTrimLeadingZeroes(t *testing.T) {
	var cases = []struct {
		a    Digits
		want Digits
	}{
		{a: Digits{0}, want: Digits{0}},
		{a: Digits{0, 0}, want: Digits{0}},
		{a: Digits{0, 1}, want: Digits{1}},
		{a: Digits{1, 2, 3}, want: Digits{1, 2, 3}},
		{a: Digits{0, 1, 2, 3}, want: Digits{1, 2, 3}},
		{a: Digits{0, 0, 1, 2, 3}, want: Digits{1, 2, 3}},
		{a: Digits{0, 0, 1, 2, 3, 0}, want: Digits{1, 2, 3, 0}},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("%v", tc.a),
			func(t *testing.T) {
				digits := tc.a.trimLeadingZeroes()
				if !reflect.DeepEqual(digits, tc.want) {
					t.Errorf(
						"Wrong output for %v, wanted: %v, got: %v\n",
						tc.a, tc.want, digits)
				}
			})
	}
}

func TestZeroPad(t *testing.T) {
	var cases = []struct {
		a    Digits
		n    int
		want Digits
	}{
		{a: Digits{0}, n: 1, want: Digits{0}},
		{a: Digits{1}, n: 1, want: Digits{1}},
		{a: Digits{1}, n: 2, want: Digits{0, 1}},
		{a: Digits{1}, n: 3, want: Digits{0, 0, 1}},
		{a: Digits{1, 0, 0}, n: 3, want: Digits{1, 0, 0}},
		{a: Digits{1, 2, 3}, n: 3, want: Digits{1, 2, 3}},
		{a: Digits{1, 2, 3}, n: 4, want: Digits{0, 1, 2, 3}},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("%v", tc.a),
			func(t *testing.T) {
				digits := tc.a.zeroPad(tc.n)
				if !reflect.DeepEqual(digits, tc.want) {
					t.Errorf(
						"Wrong output for %v, %d\nwanted: %v\n   got: %v\n",
						tc.a, tc.n, tc.want, digits)
				}
			})
	}
}
