package graph

import (
	"math"
)

func series0toN(n int) []int {
	s := []int{}
	for i := 0; i < n; i++ {
		s = append(s, i)
	}
	return s
}

func tspBrute(g *Graph, ns NodeSet) GraphPath {
	perms := NewPermutationChan(ns)

	// Initial minimum cost is infinity
	minCost := cost(math.MaxFloat64)
	minCostPath := NodeSet{}

	// Permuations are generated in lexicographical order
	for perm := range perms {
		// If the starting position isn't 0,
		// we can stop evaluating permutations
		if perm[0] != 0 {
			break
		}

		// Include the cost of the return trip
		test := append(perm, 0)
		c := g.PathCost(test)

		if c < minCost {
			minCost = c
			minCostPath = test
		}
	}

	return GraphPath{minCostPath, minCost}
}

func TSPBrute(g *Graph) GraphPath {
	return tspBrute(g, series0toN(g.NodeCount()))
}

func tspDP(g *Graph, cache NodeSetCache, ns NodeSet, startNode int, endNode int) GraphPath {
	// if res, ok := cache[ns.Packed()]; ok {
	// 	return res
	// }

	if len(ns) == 1 {
		solution := getGraphPath(g, NodeSet{startNode, ns[0], endNode})
		// cache[solution.path.Packed()] = solution
		return solution
	}

	bestGP := initGraphPath(len(ns))
	for _, k := range ns {
		candidateGP := tspDP(g, cache, ns.Without(k), k, endNode)
		if candidateGP.c < bestGP.c {
			bestGP = candidateGP
		}
	}

	// cache[bestGP.path.Packed()] = bestGP

	return bestGP
}

type NodeSetCache map[PackedNodeSet]GraphPath

func TSPDP(g *Graph) GraphPath {
	cache := NodeSetCache{}

	// // Populate the cache with costs for every 2-node tour
	// for i := 0; i < g.NodeCount(); i++ {
	// 	for j := 0; j < g.NodeCount(); j++ {
	// 		gp := getGraphPath(g, []int{i, j})
	// 		cache[gp.path.Packed()] = gp
	// 	}
	// }

	gp := tspDP(g, cache, g.NodeSet(), 0, 0)

	return gp
}

func tspGreedy(g *Graph, gp *GraphPath, currentPos int) {
	if currentPos >= len(gp.path) {
		return
	}

	// Get the next least costly node, excluding nodes already used in the path
	gp.path[currentPos] = g.LeastCostlyNodeFrom(
		gp.path[currentPos-1], gp.path[:currentPos])

	tspGreedy(g, gp, currentPos+1)
}

func TSPGreedy(g *Graph) GraphPath {
	gp := initGraphPath(g.NodeCount())

	// Find the best path
	tspGreedy(g, &gp, 1)

	// Update the path cost
	gp.c = g.PathCost(gp.path)

	// Add the return trip to origin
	appendNodeToGraphPath(g, &gp, 0)

	return gp
}
