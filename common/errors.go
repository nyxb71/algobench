package common

const (
	ErrorListWrongSize          = "List was %d elements, not %d."
	ErrorTargetNotFound         = "Target '%d' not found in list."
	ErrorWrongResultIndex       = "Search result index is not correct.\nWanted %d got %d"
	ErrorListNotSorted          = "List should be sorted but wasn't."
	ErrorListsNotMergedExpected = "Lists were not merged correctly.\nExpected %v, Got %v"
	ErrorListsNotMerged         = "Lists were not merged correctly.\n"
)
