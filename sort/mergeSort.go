package sort

func mergeRecursive(a []int, b []int) []int {
	// based on https://stackoverflow.com/questions/4574279/sorting-in-functional-programming-languages
	// Note: terrible performance due to allocation overhead

	// If either of the lists are empty, return the other list
	if len(a) == 0 {
		return b
	}
	if len(b) == 0 {
		return a
	}

	if a[0] < b[0] {
		return append([]int{a[0]}, mergeRecursive(a[1:], b)...)
	}

	return append([]int{b[0]}, mergeRecursive(a, b[1:])...)
}

// MergeSortRecursive sorts a list via recursive merges
func MergeSortRecursive(a []int) []int {
	// If the list is 0 or 1 elements, it is already sorted
	if len(a) <= 1 {
		return a
	}

	middle := len(a) / 2
	return mergeRecursive(
		MergeSortRecursive(a[:middle]),
		MergeSortRecursive(a[middle:]))
}

func mergeIterative(a []int, b []int) []int {
	// based on https://austingwalters.com/merge-sort-in-go-golang/

	// Allocate new list
	merged := make([]int, len(a)+len(b))
	i, j, current := 0, 0, 0

	// Merge a and b, taking the elements from one while they are less that the
	// current value in the other list
	for i < len(a) && j < len(b) {
		if a[i] <= b[j] {
			merged[current] = a[i]
			i++
		} else {
			merged[current] = b[j]
			j++
		}
		current++
	}

	// Append any stragglers from list a
	for i < len(a) {
		merged[current] = a[i]
		i++
		current++
	}

	// Append any stragglers from list b
	for j < len(b) {
		merged[current] = b[j]
		j++
		current++
	}

	return merged
}

// MergeSortIterative sorts a list via iterative merges
func MergeSortIterative(a []int) []int {
	// If the list is 0 or 1 elements, it is already sorted
	if len(a) <= 1 {
		return a
	}

	middle := len(a) / 2
	return mergeIterative(
		MergeSortIterative(a[:middle]),
		MergeSortIterative(a[middle:]))
}
