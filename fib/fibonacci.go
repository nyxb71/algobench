package fib

import (
	"algobench/common"
	"algobench/matrix"
	"math"
)

// FibLoop generates the Nth Fibonacci number via iteration.
func FibLoop(x uint64) uint64 {
	if x <= 1 {
		return x
	}

	secondToLast := uint64(0)
	last := uint64(1)
	current := uint64(1)
	for i := current; i < x; i++ {
		current = last + secondToLast
		secondToLast = last
		last = current
	}

	return current
}

// FibLoopDP generates the Nth Fibonacci number via iteration, uses a cache.
func FibLoopDP(x uint64) uint64 {
	if x <= 1 {
		return x
	}

	// Populate the cache
	cache := map[uint64]uint64{
		0: 0,
		1: 1,
	}

	// Calculate every Fibonacci number [2,x], store them in the cache to reuse
	for i := uint64(2); i <= x; i++ {
		cache[i] = cache[i-1] + cache[i-2]
	}

	return cache[x]
}

// FibRecur generates the Nth Fibonacci number via recursion.
func FibRecur(x uint64) uint64 {
	if x <= 1 {
		return x
	}

	return FibRecur(x-1) + FibRecur(x-2)
}

func fibRecurDP(cache map[uint64]uint64, x uint64) uint64 {
	if x <= 1 {
		return x
	}

	// If the result is in the cache, return it
	if cachedResult, ok := cache[x]; ok {
		return cachedResult
	}

	// Store non-cached results in the cache, then return that result
	cache[x] = fibRecurDP(cache, x-1) + fibRecurDP(cache, x-2)
	return cache[x]
}

// FibRecurDP returns the Nth Fibonacci number via recursion, uses a cache.
func FibRecurDP(x uint64) uint64 {
	if x <= 1 {
		return x
	}

	// Pre-populate the cache
	cache := map[uint64]uint64{
		0: 0,
		1: 1,
		2: 1,
	}

	return fibRecurDP(cache, x)
}

type matrixPowerCache map[uint64]matrix.Matrix

func fibMatrix(cache matrixPowerCache, exponent uint64) matrix.Matrix {
	// If the result is in the cache, return it
	if cachedResult, ok := cache[exponent]; ok {
		return cachedResult
	}

	// Break the exponent into two pieces
	// 7 -> (4,3)
	// 4 -> (2,2)
	// 3 -> (2,1)
	// 1 -> (1,0)
	// 0 -> (0,0)
	expA, expB := common.SplitExponent(exponent)

	// Calculate FibM^expA * FibM*expB
	res, _ := matrix.Mult2x2(
		fibMatrix(cache, expA),
		fibMatrix(cache, expB))

	// Store the result in the cache, then return it
	cache[exponent] = res
	return cache[exponent]
}

// FibMatrix returns the Nth Fibonacci number
// via recursive matrix multiplication and caching
func FibMatrix(x uint64) uint64 {
	if x <= 1 {
		return x
	}

	// Populate the cache
	FibM, _ := matrix.From2DArray([][]uint64{{1, 1}, {1, 0}})
	FibM2, _ := matrix.Mult2x2(FibM, FibM)
	cache := matrixPowerCache{
		1: FibM,
		2: FibM2,
	}

	// Calculate FibM^(x-1)
	res := fibMatrix(cache, x-1)
	// Get the top-left element, which is Fib(x)
	return res.Cell(0, 0)
}

func FibFormula(x uint64) uint64 {
	// Accurate for x < 76
	// https://artofproblemsolving.com/wiki/index.php?title=Binet%27s_Formula
	// https://bosker.wordpress.com/2011/07/27/computing-fibonacci-numbers-using-binet%E2%80%99s-formula/
	if x <= 1 {
		return x
	}

	sqrt5 := math.Sqrt(5)
	a := (math.Pow(((1 + sqrt5) / 2), float64(x)))
	b := (math.Pow(((1 - sqrt5) / 2), float64(x)))

	fibn := (1 / sqrt5) * (a - b)

	// Round instead of floor here gets more accurate results,
	// since some values of fibn have decimal components close to 1
	// Ex x.9999999....
	return uint64(math.Round(fibn))
}
