package str

import (
	"fmt"
	"testing"
)

func TestWordStringRandom(t *testing.T) {
	rws := RandomWordString(100)

	if len(rws) != 100 {
		t.Errorf("random WordString had wrong length")
	}

	_, err := ParseWordString(string(rws))
	if err != nil {
		t.Errorf("randomly generator WordString did not parse as a valid WordString")
	}
}

func TestWordStringRandomSlice(t *testing.T) {
	var cases = []struct {
		a    string
		len  uint64
		want string
	}{
		{a: "foobarbaz", len: 8, want: "foobarbaz"},
		{a: "foobarbaz", len: 0, want: ""},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("%v", tc),
			func(t *testing.T) {
				slice, err := WordString(tc.a).RandomSlice(tc.len)
				if err != nil {
					t.Error(err)
				}
				if len(slice) != int(tc.len) {
					t.Errorf("slice was wrong length, wanted %d, got %d", tc.len, len(slice))
				}
			})
	}
}
